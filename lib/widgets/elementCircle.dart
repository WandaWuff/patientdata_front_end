import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class ElementCircle extends StatelessWidget {
  final Widget child;
  final FileImage backGroundImage;
  final String character;
  final double size;

  ElementCircle({this.child, this.backGroundImage, this.character, this.size});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(right: MySpacer.XL),
      child: CircleAvatar(
        radius: 28,
        backgroundColor: Theme.of(context).platform == TargetPlatform.iOS
            ? MyColors.PrimaryColor
            : MyColors.White,
        backgroundImage: backGroundImage,
        child: character == null
            ? child
            : Text(
                character,
                style: TextStyle(
                  fontSize: size == null ? 30.0 : size,
                  color: MyColors.PrimaryColor,
                ),
              ),
      ),
    );
  }
}
