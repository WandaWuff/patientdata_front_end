import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/appLocalizations.dart';

class RadioGroup extends StatelessWidget {
  final int groupValue;
  final Function onChanged;

  RadioGroup({
    @required this.groupValue,
    @required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Row(
          children: [
            Radio(
              value: 0,
              groupValue: groupValue,
              onChanged: onChanged,
            ),
            MainText(AppLocalizations.of(context).get('female')),
          ],
        ),
        Row(
          children: [
            Radio(
              value: 1,
              groupValue: groupValue,
              onChanged: onChanged,
            ),
            MainText(AppLocalizations.of(context).get('male')),
          ],
        ),
        Row(
          children: [
            Radio(
              value: 2,
              groupValue: groupValue,
              onChanged: onChanged,
            ),
            MainText(AppLocalizations.of(context).get('divers')),
          ],
        ),
      ],
    );
  }
}
