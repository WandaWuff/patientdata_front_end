import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/spacer.dart';

class DataElement extends StatelessWidget {
  final String element;
  final String value;

  DataElement({@required this.element, this.value,});

  @override
  Widget build(BuildContext context) {
    return value != null
        ? Padding(
            padding: const EdgeInsets.all(MySpacer.XL),
            child: Row(
              children: <Widget>[
                MainText(element + ': '),
                MainText(value),
              ],
            ),
          )
        : Padding(
            padding: const EdgeInsets.all(MySpacer.XL),
            child: MainText(element),
          );
  }
}
