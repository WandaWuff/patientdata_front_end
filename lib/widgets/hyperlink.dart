import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';

class Hyperlink extends StatelessWidget {
  final String text;
  final Function function;

  Hyperlink({this.text, this.function});
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: MainText(text),
      onTap: function,
    );
  }
}
