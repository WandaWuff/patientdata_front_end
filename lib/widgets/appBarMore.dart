import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/moreMenuItem.dart';

class AppBarMore extends StatefulWidget {
  final Function onSelected;
  final List<MoreMenuItem> menus;

  AppBarMore({this.onSelected, this.menus});

  @override
  _AppBarMoreState createState() => _AppBarMoreState();
}

class _AppBarMoreState extends State<AppBarMore> {
  @override
  Widget build(BuildContext context) {
    return PopupMenuButton<String>(
      color: MyColors.White,
      icon: Icon(Icons.more_vert),
      elevation: 5,
      onSelected: widget.onSelected,
      itemBuilder: (BuildContext context) {
        return widget.menus.map((MoreMenuItem item) {
          return PopupMenuItem<String>(
            value: item.text,
            child: ListTile(
              leading: Icon(
                item.icon,
                color: Colors.black,
              ),

              title: Text(item.text),
            ),
          );
        }).toList();
      },
    );
  }
}
