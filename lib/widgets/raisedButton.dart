import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class MyRaisedButton extends StatelessWidget {
  final String text;
  final IconData icon;
  final Function function;
  final Color color;
  final EdgeInsets padding;

  MyRaisedButton(
      {@required this.text,
      this.icon,
      @required this.function,
      this.color,
      this.padding});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(MySpacer.L),
      child: RaisedButton(
        child: Padding(
          padding: padding == null ? EdgeInsets.all(0) : padding,
          child: icon == null
              ? Text(text)
              : Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(MySpacer.M),
                      child: Icon(icon),
                    ),
                    Text(text),
                  ],
                ),
        ),
        onPressed: function,
        color: color == null ? MyColors.PrimaryAccentColor : color,
        textColor: color == null ? MyColors.White : Colors.black,
      ),
    );
  }
}
