import 'package:flutter/material.dart';

class MoreMenuItem {
  IconData icon;
  String text;

  MoreMenuItem({this.icon, this.text});
}