import 'package:flutter/material.dart';

class Header extends StatelessWidget {
  final String text;
  final Color color;

  Header(this.text, {this.color});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: 18,
        fontWeight: FontWeight.bold,
      ),
    );
  }
}
