import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class MyTextInput extends StatelessWidget {
  final String label;
  final String hint;
  final Function function;
  final inputController;
  final bool number;
  final int maxChar;
  final TextInputAction inputAction;
  final FocusNode focusNode;
  final Function onFieldSubmitted;
  final Function onChanged;
  final bool obscureText;
  final int maxLines;
  final String error;

  MyTextInput({
    this.label,
    this.hint,
    this.function,
    this.inputController,
    this.number,
    this.maxChar,
    this.focusNode,
    this.onFieldSubmitted,
    this.inputAction,
    this.onChanged,
    this.obscureText,
    this.maxLines,
    this.error,
  });

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: MySpacer.L),
      child: TextField(
        decoration: InputDecoration(
          errorText: error,
          labelText: label,
          enabledBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MyColors.PrimaryAccentColor),
          ),
          focusedBorder: UnderlineInputBorder(
            borderSide: BorderSide(color: MyColors.PrimaryAccentColor),
          ),
        ),
        inputFormatters: [
          LengthLimitingTextInputFormatter(maxChar),
        ],
        keyboardType: number == null
            ? TextInputType.text
            : TextInputType.numberWithOptions(decimal: number),
        controller: inputController,
        maxLines: maxLines,
        textInputAction: inputAction,
        focusNode: focusNode,
        onSubmitted: onFieldSubmitted,
        textCapitalization: TextCapitalization.sentences,
        onChanged: onChanged,
        obscureText: obscureText == null ? false : obscureText,
      ),
    );
  }
}
