import 'package:flutter/material.dart';
import 'package:patientdata/widgets/header.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/appLocalizations.dart';

class MyDialog {
  static showDialogWithParam(BuildContext context, String message) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Header(AppLocalizations.of(context).get('caution') + '!'),
          content: MainText(message),
          actions: <Widget>[
            new FlatButton(
              child: new Text(AppLocalizations.of(context).get('close')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static showDialogValidInput(BuildContext context, String input) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Header(AppLocalizations.of(context).get('caution') + '!'),
          content: MainText(AppLocalizations.of(context).get('wrongInput') + ' : $input'),
          actions: <Widget>[
            new FlatButton(
              child: new Text(AppLocalizations.of(context).get('close')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static showDialogWithNoEmptyFields(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return AlertDialog(
          title: Header(AppLocalizations.of(context).get('caution') + '!'),
          content: MainText(AppLocalizations.of(context).get('noEmpty') + '!'),
          actions: <Widget>[
            new FlatButton(
              child: new Text(AppLocalizations.of(context).get('close')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
          ],
        );
      },
    );
  }

  static showDialogWithDeleteFunction(
      BuildContext context, String message, Function function) {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return AlertDialog(
          title: Header(AppLocalizations.of(context).get('caution') + '!'),
          content: MainText(message),
          actions: <Widget>[
            new FlatButton(
              child: new Text(AppLocalizations.of(context).get('no')),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new FlatButton(
              child: new Text(AppLocalizations.of(context).get('yes')),
              onPressed: function,
            ),
          ],
        );
      },
    );
  }
}
