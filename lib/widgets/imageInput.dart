import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'dart:io';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as sysPaths;

class ImageInput extends StatefulWidget {
  final Function onSelectImage;
  final String text;
  final File file;

  ImageInput(this.onSelectImage, this.text, {this.file});

  @override
  _ImageInputState createState() => _ImageInputState();
}

class _ImageInputState extends State<ImageInput> {
  File _storedImage;
  final picker = ImagePicker();

  Future<void> _takePicture() async {
    final imageFile = await picker.getImage(
      source: ImageSource.gallery,
      //source: ImageSource.camera,
      maxWidth: 600,
    );
    if (imageFile == null) {
      return;
    }
    setState(() {
      _storedImage = File(imageFile.path);
    });
    final appDir = await sysPaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    final savedImage = await _storedImage.copy('${appDir.path}/$fileName');
    widget.onSelectImage(savedImage);
  }

  @override
  void initState() {
    if (widget.file != null) {
      setState(() {
        _storedImage = widget.file;
      });
    }

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(MySpacer.L),
      child: Row(
        children: <Widget>[
          Container(
            width: 100,
            height: 150,
            decoration: BoxDecoration(
              border: Border.all(width: 1, color: MyColors.PrimaryAccentColor),
            ),
            child: _storedImage != null
                ? Image.file(
                    _storedImage,
                    fit: BoxFit.cover,
                    width: double.infinity,
                  )
                : Icon(Icons.block),
            alignment: Alignment.center,
          ),
          SizedBox(
            width: 10,
          ),
          Expanded(
            child: MyRaisedButton(
              text: widget.text,
              icon: Icons.camera,
              function: _takePicture,
            ),
          ),
        ],
      ),
    );
  }
}
