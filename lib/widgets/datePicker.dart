import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';

class DatePicker {
  //TODO: wtf is happening with manual date?
  static Future<DateTime> presentDatePicker(
    BuildContext context,
    DateTime initialDate,
    DateTime firstDate,
    DateTime lastDate,
  ) {
    return showDatePicker(
      fieldHintText: "TT.MM.JJJJ",
      fieldLabelText: "Startdatum",
      helpText: "Datum auswählen",
      errorFormatText: "Ungültiges Format",
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: MyColors.PrimaryColor,
            accentColor: MyColors.PrimaryAccentColor,
            cardColor: MyColors.PrimaryColorExtraLight,
            colorScheme: ColorScheme.light(primary: MyColors.PrimaryColor),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
      context: context,
      initialDate: initialDate,
      firstDate: firstDate == null ? DateTime(1900) : firstDate,
      lastDate: lastDate,
      locale: const Locale('de', 'DE'),
    );
  }
}
