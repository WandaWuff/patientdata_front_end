import 'package:flutter/material.dart';

import '../../values/colors.dart';

class UserAccountHeader extends StatelessWidget {
  final String name;
  final String email;

  UserAccountHeader(this.name, this.email);

  @override
  Widget build(BuildContext context) {
    return UserAccountsDrawerHeader(
      accountName: Text(name),
      accountEmail: Text(email),
      currentAccountPicture: CircleAvatar(
        backgroundColor:
        Theme.of(context).platform == TargetPlatform.iOS
            ? MyColors.PrimaryColor
            : MyColors.White,
        child: Text(
          name.substring(0, 1),
          style: TextStyle(fontSize: 40.0),
        ),
      ),
    );
  }
}
