import 'package:flutter/material.dart';
import 'package:patientdata/widgets/drawer/userAccountHeader.dart';
import 'package:patientdata/routes/routes.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'drawerItem.dart';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  String username = '-';
  String name = '-';

  final drawerItems = [
    DrawerItem(
      title: 'Overview',
      icon: Icons.contact_mail,
    ),
    DrawerItem(
      title: 'Emergency contacts',
      icon: Icons.contacts,
    ),
    DrawerItem(
      title: 'Diagnoses',
      icon: Icons.assignment,
    ),
    DrawerItem(
      title: 'Documents',
      icon: Icons.folder,
    ),
    DrawerItem(
      title: 'Settings',
      icon: Icons.settings,
    ),
  ];

  _onSelectItem(int pos) {
    switch (pos) {
      case 0:
        Navigator.pushReplacementNamed(context, Routes.baseData);
        break;
      case 1:
        Navigator.pushReplacementNamed(context, Routes.emergencyView);
        break;
      case 2:
        Navigator.pushReplacementNamed(context, Routes.diagnosis);
        break;
      case 3:
        Navigator.pushReplacementNamed(context, Routes.documents);
        break;
      case 4:
        Navigator.pushReplacementNamed(context, Routes.settingView);
        break;
      default:
        return Text("Error");
    }
  }

  void _getPatient() async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String pfTenant = pf.getString('username');
    String pfName = pf.getString('name');
    String tenant = pf.getString('tenant');

    if (pfTenant != null && pfName != null) {
      if (pfTenant == tenant) {
        setState(() {
          username = pfTenant;
          name = pfName;
        });
      } else {
        setState(() {
          username = pfTenant;
          name = 'Patient: $pfName';
        });
      }
    }
  }

  @override
  void initState() {
    _getPatient();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    List<Widget> drawerOptions = [];

    for (var i = 0; i < drawerItems.length; i++) {
      var d = drawerItems[i];
      drawerOptions.add(Column(
        children: <Widget>[
          ListTile(
            leading: new Icon(d.icon),
            title: new Text(d.title),
            onTap: () => _onSelectItem(i),
          ),
          Divider(
            height: 0.0,
            color: MyColors.PrimaryColor,
          ),
        ],
      ));
    }

    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          UserAccountHeader(name, username),
          MyDivider(EdgeInsets.all(0)),
          Column(
            children: drawerOptions,
          ),
        ],
      ),
    );
  }
}
