import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';

class MyDivider extends StatelessWidget {
  final EdgeInsetsGeometry padding;

  MyDivider(this.padding);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Divider(
        height: 0.0,
        color: MyColors.PrimaryColor,
      ),
    );
  }
}
