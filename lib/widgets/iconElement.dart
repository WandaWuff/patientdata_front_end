import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/spacer.dart';

class IconElement extends StatelessWidget {
  final IconData icon;
  final String value;

  IconElement({@required this.icon, @required this.value,});

  @override
  Widget build(BuildContext context) {
    return Padding(
            padding: const EdgeInsets.all(MySpacer.XL),
            child: Row(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(right: MySpacer.L),
                  child: Icon(icon),
                ),
                MainText(value),
              ],
            ),
          );
  }
}
