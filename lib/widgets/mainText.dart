import 'package:flutter/material.dart';

class MainText extends StatelessWidget {
  final String text;
  final Color color;
  final TextAlign align;

  MainText(this.text, {this.color, this.align});

  @override
  Widget build(BuildContext context) {
    return Text(
      text,
      style: TextStyle(
        color: color,
        fontSize: 16,
      ),
      maxLines: null,
      textAlign: align,
    );
  }
}
