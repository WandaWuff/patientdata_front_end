import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';

class EmptyList extends StatelessWidget {
  final String message;

  EmptyList(this.message);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(MySpacer.XL),
            child: Icon(
              Icons.block,
              size: 40,
            ),
          ),
          MainText('$message ' + AppLocalizations.of(context).get('notAvailable')),
        ],
      ),
    );
  }
}
