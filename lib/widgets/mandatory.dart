import 'package:flutter/material.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class Mandatory extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: MySpacer.L),
      child: Text(
        '*' +  AppLocalizations.of(context).get('required'),
        style: TextStyle(color: MyColors.PrimaryAccentColor),
      ),
    );
  }
}
