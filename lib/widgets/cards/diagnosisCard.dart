import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/elementCircle.dart';
import 'package:provider/provider.dart';
import '../header.dart';
import '../mainText.dart';
import '../../values/spacer.dart';
import 'package:patientdata/models/diagnosis.dart';

// ignore: must_be_immutable
class DiagnosisCard extends StatefulWidget {
  Diagnosis _diagnosis;
  final Function onTap;
  final Function longTap;
  final Function goTo;
  final bool filter;
  final int index;
  final bool longPressEnabled;
  final VoidCallback callback;
  final Function() notifyParent;

  DiagnosisCard(
    this._diagnosis, {
    this.onTap,
    this.longTap,
    this.longPressEnabled,
    this.index,
    this.goTo,
    this.filter,
    this.callback,
    this.notifyParent,
  });

  @override
  _DiagnosisCardState createState() => _DiagnosisCardState();
}

class _DiagnosisCardState extends State<DiagnosisCard> {
  bool selected = false;
  String icdText;

  Future<String> fetchICD(String icd, BuildContext context) async {
    String localText =
        await Provider.of<ApiProvider>(context, listen: false).fetchICD(icd);

    if (localText != null) {
      setState(() {
        icdText = localText;
        widget._diagnosis.setText(icdText);
      });

      return localText;
    } else {
      throw Exception(AppLocalizations.of(context).get('failedIcd'));
    }
  }

  @override
  void initState() {
    fetchICD(widget._diagnosis.icd, context);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String formattedDate =
        DateFormat('dd.MM.yy').format(widget._diagnosis.date);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Card(
          color: selected && widget.longPressEnabled
              ? MyColors.PrimaryColorLight
              : null,
          elevation: 20,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(color: MyColors.PrimaryDarkColor)),
          child: InkWell(
            onLongPress: () {
              if (widget.filter) {
                Scaffold.of(context).showSnackBar(new SnackBar(
                  content: new Text(
                      AppLocalizations.of(context).get('unableFunction')),
                ));
              } else {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              }
            },
            onTap: () {
              if (widget.longPressEnabled) {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              } else {
                widget.goTo();
              }
            },
            child: Container(
              padding: EdgeInsets.all(MySpacer.XL),
              child: Row(
                children: <Widget>[
                  ElementCircle(
                      character: widget._diagnosis.icd.substring(0, 3),
                      size: 20.0),
                  Container(
                    child: Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          icdText == null ? Container() : Header('$icdText'),
                          MainText('${widget._diagnosis.doctor} ' +
                              AppLocalizations.of(context).get('from') +
                              ' $formattedDate'),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
