import 'package:flutter/material.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/elementCircle.dart';
import '../header.dart';
import '../mainText.dart';
import '../../values/spacer.dart';
import 'package:patientdata/models/emergencyContact.dart';

// ignore: must_be_immutable
class EmergencyContactCard extends StatefulWidget {
  EmergencyContact _emergencyContact;
  final Function onTap;
  final Function longTap;
  final Function goTo;
  final bool filter;
  final int index;
  final bool longPressEnabled;
  final VoidCallback callback;
  final Function() notifyParent;

  EmergencyContactCard(this._emergencyContact,
      {this.onTap,
      this.longTap,
      this.longPressEnabled,
      this.index,
      this.goTo,
      this.filter,
      this.callback,
      this.notifyParent});

  @override
  _EmergencyContactCardState createState() => _EmergencyContactCardState();
}

class _EmergencyContactCardState extends State<EmergencyContactCard> {
  bool selected = false;

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Card(
          color: selected && widget.longPressEnabled
              ? MyColors.PrimaryColorLight
              : null,
          elevation: 20,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(color: MyColors.PrimaryDarkColor)),
          child: InkWell(
            onLongPress: () {
              if (widget.filter) {
                Scaffold.of(context).showSnackBar(new SnackBar(
                  content: new Text(
                      AppLocalizations.of(context).get('unableFunction')),
                ));
              } else {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              }
            },
            onTap: () {
              if (widget.longPressEnabled) {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              } else {
                widget.goTo();
              }
            },
            child: Container(
              padding: EdgeInsets.all(MySpacer.XL),
              child: Row(
                children: <Widget>[
                  widget._emergencyContact.lastName.isNotEmpty
                      ? ElementCircle(
                          character:
                              widget._emergencyContact.lastName.substring(0, 1))
                      : ElementCircle(child: Icon(Icons.person)),
                  Container(
                    child: Flexible(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Header(
                              '${widget._emergencyContact.getFullName()}'),
                          widget._emergencyContact.email.isEmpty
                              ? Container()
                              : MainText('${widget._emergencyContact.email}')
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
