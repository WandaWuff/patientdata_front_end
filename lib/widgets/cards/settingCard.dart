import 'package:flutter/material.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class SettingCard extends StatelessWidget {
  final String setting;
  final String detail;
  final Function onTap;
  final IconData icon;

  SettingCard({
    this.setting,
    this.detail,
    this.onTap,
    this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Card(
      color: MyColors.White,
      elevation: 20,
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(5.0),
          side: BorderSide(color: MyColors.PrimaryDarkColor)),
      child: InkWell(
        onTap: onTap,
        child: Container(
          padding: EdgeInsets.all(MySpacer.XL),
          child: Row(
            children: <Widget>[
              Expanded(
                flex: 7,
                child: Container(
                  child: Row(
                    children: <Widget>[
                      CircleAvatar(
                        radius: 23,
                        backgroundColor:
                            Theme.of(context).platform == TargetPlatform.iOS
                                ? Colors.black
                                : MyColors.PrimaryDarkColor,
                        child: Icon(
                          icon,
                          color: MyColors.White,
                        ),
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: MySpacer.XL),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              MainText(
                                setting,
                                color: MyColors.PrimaryDarkColor,
                              ),
                              detail == null ? Container() : Padding(
                                padding:
                                    const EdgeInsets.only(top: MySpacer.M),
                                child: Text(
                                  detail,
                                  style: TextStyle(
                                      color: MyColors.PrimaryColorExtraLight,
                                      fontSize: 12),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
