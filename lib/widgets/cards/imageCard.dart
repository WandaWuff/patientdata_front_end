import 'dart:io';

import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/detailScreen.dart';

class ImageCard extends StatelessWidget {
  final File file;

  ImageCard(this.file);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: Card(
        elevation: 20,
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(5.0),
            side: BorderSide(color: MyColors.PrimaryDarkColor)),
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: Image.file(
            file,
            // width: 300,
            height: 150,
            fit: BoxFit.cover,
          ),
        ),
      ),
      onTap: () {
        Navigator.push(context, MaterialPageRoute(builder: (_) {
          return DetailScreen(file);
        }));
      },
    );
  }
}
