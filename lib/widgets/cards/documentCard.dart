import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/widgets/elementCircle.dart';
import '../header.dart';
import '../mainText.dart';
import '../../values/spacer.dart';

class DocumentCard extends StatefulWidget {
  final Document document;
  final Function onTap;
  final Function longTap;
  final Function goTo;
  final bool filter;
  final int index;
  final bool longPressEnabled;
  final VoidCallback callback;

  DocumentCard(
      {this.document,
      this.onTap,
      this.longTap,
      this.goTo,
      this.filter,
      this.index,
      this.longPressEnabled,
      this.callback});

  @override
  _DocumentCardState createState() => _DocumentCardState();
}

class _DocumentCardState extends State<DocumentCard> {
  bool selected = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String formattedDate = DateFormat('dd.MM.yy').format(widget.document.date);

    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Card(
          color: selected && widget.longPressEnabled
              ? MyColors.PrimaryColorLight
              : null,
          elevation: 20,
          shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5.0),
              side: BorderSide(color: MyColors.PrimaryDarkColor)),
          child: InkWell(
            onLongPress: () {
              if (widget.filter) {
                Scaffold.of(context).showSnackBar(new SnackBar(
                  content: new Text(
                      AppLocalizations.of(context).get('unableFunction')),
                ));
              } else {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              }
            },
            onTap: () {
              if (widget.longPressEnabled) {
                setState(() {
                  selected = !selected;
                });
                widget.callback();
              } else {
                widget.goTo();
              }
            },
            child: Container(
              padding: EdgeInsets.all(MySpacer.XL),
              child: Row(
                children: <Widget>[
                  ElementCircle(child: Icon(Icons.photo)),
                  Flexible(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Header('${widget.document.title}'),
                        MainText('$formattedDate'),
                        MainText(
                            Validation.cutText(widget.document.description)),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }
}
