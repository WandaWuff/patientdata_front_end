import 'package:flutter/material.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';

class DateButton extends StatefulWidget {
  final String text;
  final IconData icon;
  final Function function;
  final TextAlign alignment;

  DateButton({
    this.icon,
    this.function,
    this.alignment,
    this.text,
  });

  @override
  _DateButtonState createState() => _DateButtonState();
}

class _DateButtonState extends State<DateButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(MySpacer.L),
      child: Ink(
        decoration: BoxDecoration(
          border: Border(
            bottom: BorderSide(
              color: MyColors.PrimaryAccentColor,
            ),
          ),
        ),
        child: InkWell(
          onTap: widget.function,
          child: Padding(
            padding: const EdgeInsets.only(
                right: MySpacer.M, top: MySpacer.XXL - 3, bottom: MySpacer.XXL),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(
                      right: MySpacer.M, top: MySpacer.M, bottom: MySpacer.M),
                  child: Icon(Icons.calendar_today),
                ),
                Text(
                  widget.text,
                  textAlign: widget.alignment,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
