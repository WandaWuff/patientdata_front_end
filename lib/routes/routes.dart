import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/views/createUserView.dart';
import 'package:patientdata/views/diagnosis/diagnosisList.dart';
import 'package:patientdata/views/documents/documentsList.dart';
import 'package:patientdata/views/emergencyContacts/emergencyContactList.dart';
import 'package:patientdata/views/logInView.dart';
import 'package:patientdata/views/settingView.dart';

class Routes {
  static const String baseData = BaseDataOverview.routeName;
  static const String diagnosis = DiagnosisList.routeName;
  static const String documents = DocumentsList.routeName;
  static const String settingView = SettingView.routeName;
  static const String emergencyView = EmergencyContactList.routeName;
  static const String loginView = LogInView.routeName;
  static const String createUserView = CreateUserView.routeName;

}