class Validation {
  static String trimLastSpace(String string) {
    if (string.endsWith(' ')) {
      return string.substring(0, string.length - 1);
    } else {
      return string;
    }
  }

  static String replaceComma(String string) {
    if (string.contains(',')) {
      return string.replaceAll(',', '.');
    } else {
      return string;
    }
  }

  static bool validPlz(String plz) {
    if (plz.length != 5) {
      return false;
    } else {
      return true;
    }
  }

  static bool validEmail(String email) {
    if (email.contains('@')) {
      return true;
    } else {
      return false;
    }
  }

  static String cutText(String text) {
    if (text.length > 100) {
      return text.substring(0, 100) + '...';
    } else {
      return text;
    }
  }

}