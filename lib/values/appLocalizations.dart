import 'dart:async';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class AppLocalizations {
  final Locale locale;

  AppLocalizations(this.locale);

  //Helper function
  static AppLocalizations of(BuildContext context) {
    return Localizations.of<AppLocalizations>(context, AppLocalizations);
  }

  static Map<String, Map<String, String>> _localizedValues = {
    'en': {
      'failed': 'Ups, something went wrong',
      'appTitle': 'Precious health data',
      'shortDesc':
          'Precious health data app for sharing health related data with your physician.',
      'noAccount': 'No account? Tap here.',
      'closeApp': 'Tap again to close the app.',
      'required': 'Required',
      'signOutQuestion': 'Are you sure you want to log off?',
      'wrongCredentials': 'Wrong credentials.',
      'editingFailed': 'Editing failed.',
      //Buttons
      'shareButton': 'Share',
      'saveButton': 'Save',
      'searchDiagnosisButton': 'Search for diagnosis',
      'removePictureButton': 'Remove picture',
      'registerButton': 'Register',
      'addUserButton': 'Add new user',
      //Dialog
      'caution': 'Caution',
      'close': 'Close',
      'noEmpty': 'Fill in all required fields',
      'wrongInput': 'Wrong input for',
      'yes': 'Yes',
      'no': 'No',
      //Basedata
      'basedata': 'Overview',
      'firstName': 'Firstname',
      'lastName': 'Lastname',
      'name': 'Name',
      'gender': 'Gender',
      'bloodFormula': 'Blood group',
      'dob': 'Date of birth',
      'cob': 'City of birth',
      'address': 'Address',
      'insurance': 'Healthinsurance',
      'insuranceNumber': 'Insurancenumber',
      'email': 'E-Mail',
      'insertBasedata': 'Please insert basedata',
      'in': 'in',
      //Basedata edit
      'apiNotReachable': 'REST-Api not reachable.',
      'plzNotExist': 'PLZ does not exist.',
      'plz': 'Postal code',
      'female': 'female',
      'male': 'male',
      'divers': 'divers',
      'basedataEdit': 'Basedata edit',
      'streetHouse': 'Street and housenumber',
      'city': 'City',
      //DiagnosisAdd
      'noIcd': 'No ICD found.',
      'foundDiagnosis': 'Found diagnosis.',
      'failedIcd': 'Failed to load icd.',
      'addDiagnosis': 'Add diagnosis',
      'icd': 'ICD-Code',
      'diagnosis': 'Diagnosis',
      'diagnosisMore': 'Diagnosis',
      'physician': 'Physician',
      'clinic': 'Clinic',
      'diagnosedOn': 'Diagnosed on',
      'comment': 'Comment',
      'title': 'Title',
      //Diagnosis detail
      'detail': 'Detail',
      'forSureDelete': 'Are you sure to delete this diagnosis?',
      'unableToLoadPicture': 'Unable to load picture.',
      //Diagnosis edit
      'editDiagnosis': 'Diagnose bearbeiten',
      //Diagnosis element
      'unableFunction': 'Function not available.',
      'treated': 'Treated by',
      //Diagnosis list
      'sortedByAbc': 'Sorted by alphabet',
      'sortedByDate': 'Sorted by date',
      'selected': 'selected',
      'forSureDeleteMore': 'Are you sure to delete these diagnosis?',
      'keyword': 'Keyword',
      //Documents add
      'addDocument': 'Add document',
      'description': 'Description',
      'documentDate': 'Date of document',
      'changePicture': 'Change picture',
      'takePicture': 'Select picture',
      'takeMorePicture': 'Take another picture',
      //Documents detail
      'deleteDocument': 'Are you sure to delete this document?',
      'from': 'of',
      //Documents edit
      'editDocument': 'Edit document',
      //Documents list
      'documents': 'Documents',
      'deleteDocuments': 'Are you sure to delete these documents?',
      //empty list
      'notAvailable': 'not available',
      //Settings
      'settings': 'Settings',
      'logOff': 'Log off',
      //Emergency contacts
      'emergencyContact': 'Emergency contact',
      'emergencyContacts': 'Emergency contacts',
      'addEmergencyContact': 'Add emergency contact',
      'emergencyContactEdit': 'Edit emergency contact',
      'phone': 'Phone number',
      'deleteEmergencyContact':
          'Are you sure to delete this emergency contact?',
      'emergencyContactMore':
          'Are you sure to delete these emergency contacts?',
      //Login View
      'userName': 'Username',
      'password': 'Password',
      'domain': 'Domain',
      'logIn': 'Log in',
      //Register View
      'createAccount': 'Create account',
      'againPassword': 'Repeat password',
      'accountExists': 'You have an account? Tap here.',
      'passwordNotSame': 'Passwords are not the same.',
      //Add new User
      'addUser': 'Add new user',
      'newUserAdded': 'Added new user',
      'minChar': 'Password too short.',
      //add document
      'author': 'Author',
    },
    'de': {
      'failed': 'Ups, hier ist etwas schief gegangen',
      'appTitle': 'Gesundheitsdaten',
      'shortDesc':
          'Super coole App für deine Gesundheitsdaten. Bestimme selbst, wer Zugriff darauf hat.',
      'noAccount': 'Kein Account? Klicke hier.',
      'closeApp': 'Erneut drücken, um die App zu schließen.',
      'required': 'Pflichtfelder',
      'signOutQuestion': 'Willst du dich wirklich abmelden?',
      'wrongCredentials': 'E-Mail oder Passwort falsch.',
      'editingFailed': 'Bearbeiten fehlgeschlagen.',
      //Buttons
      'shareButton': 'Freigeben',
      'saveButton': 'Speichern',
      'searchDiagnosisButton': 'Diagnose suchen',
      'removePictureButton': 'Bild entfernen',
      'registerButton': 'Registrieren',
      'addUserButton': 'Neuen User anlegen',
      //Dialog
      'caution': 'Achtung',
      'close': 'Schließen',
      'noEmpty': 'Bitte keine Pflichtfelder leer lassen',
      'wrongInput': 'Falscher Wert für',
      'yes': 'Ja',
      'no': 'Nein',
      //Basedata
      'basedata': 'Übersicht',
      'firstName': 'Vorname',
      'lastName': 'Nachname',
      'name': 'Name',
      'gender': 'Geschlecht',
      'bloodFormula': 'Blutgruppe',
      'dob': 'Geburtstag',
      'cob': 'Geburtsort',
      'address': 'Anschrift',
      'insurance': 'Krankenversicherung',
      'insuranceNumber': 'Versichertennummer',
      'email': 'E-Mail',
      'insertBasedata': 'Bitte Stammdaten eintragen',
      'in': 'in',
      //Basedata edit
      'apiNotReachable': 'REST-Api nicht erreichbar.',
      'plzNotExist': 'PLZ existiert nicht.',
      'plz': 'PLZ',
      'female': 'weiblich',
      'male': 'männlich',
      'divers': 'divers',
      'basedataEdit': 'Stammdaten ändern',
      'streetHouse': 'Straße und Hausnummer',
      'city': 'Stadt',
      //DiagnosisAdd
      'noIcd': 'Keine ICD gefunden.',
      'foundDiagnosis': 'Passende Diagnose gefunden.',
      'failedIcd': 'ICD fehlgeschlagen',
      'addDiagnosis': 'Diagnose hinzufügen',
      'icd': 'ICD-Code',
      'diagnosis': 'Diagnose',
      'diagnosisMore': 'Diagnosen',
      'physician': 'Arzt',
      'clinic': 'Praxis/Klinik',
      'diagnosedOn': 'Diagnostiziert am',
      'comment': 'Kommentar',
      'title': 'Titel',
      //Diagnosis detail
      'detail': 'Detail',
      'forSureDelete': 'Soll diese Diagnose wirklich gelöscht werden?',
      'unableToLoadPicture':
          'Bild konnte nicht geladen werden oder wurde gelöscht.',
      //Diagnosis edit
      'editDiagnosis': 'Diagnose bearbeiten',
      //Diagnosis element
      'unableFunction': 'Funktion hier nicht verfügbar.',
      'treated': 'Behandelt von',
      //Diagnosis list
      'sortedByAbc': 'Nach dem Alphabet sortiert',
      'sortedByDate': 'Nach Datum sortiert',
      'selected': 'ausgewählt',
      'forSureDeleteMore': 'Sollen diese Diagnosen wirklich gelöscht werden?',
      'keyword': 'Suchwort',
      //Documents add
      'addDocument': 'Dokument hinzufügen',
      'description': 'Beschreibung',
      'documentDate': 'Datum des Dokuments',
      'changePicture': 'Bild ändern',
      'takePicture': 'Bild auswählen',
      'takeMorePicture': 'Weiteres Bild aufnehmen',
      //Documents detail
      'deleteDocument': 'Soll dieses Dokument wirklich gelöscht werden?',
      'from': 'vom',
      //Documents edit
      'editDocument': 'Dokument bearbeiten',
      //Documents list
      'documents': 'Dokumente',
      'deleteDocuments': 'Sollen diese Dokumente wirklich gelöscht werden?',
      //empty list
      'notAvailable': 'nicht vorhanden',
      //Settings
      'settings': 'Einstellungen',
      'logOff': 'Abmelden',
      //Emergency contacts
      'emergencyContact': 'Notfallkontakt',
      'emergencyContacts': 'Notfallkontakte',
      'addEmergencyContact': 'Notfallkontakt hinzufügen',
      'emergencyContactEdit': 'Notfallkontakt bearbeiten',
      'phone': 'Telefonnummer',
      'deleteEmergencyContact':
          'Soll dieser Notfallkontakt wirklich gelöscht werden?',
      'emergencyContactMore':
          'Sollen diese Notfallkontakte wirklich gelöscht werden?',
      //Login View
      'userName': 'Username',
      'password': 'Passwort',
      'domain': 'Domäne',
      'logIn': 'Einloggen',
      //Register View
      'createAccount': 'Account erstellen',
      'againPassword': 'Passwort wiederholen',
      'accountExists': 'Account vorhanden? Klicke hier.',
      'passwordNotSame': 'Passwörter stimmen nicht überein.',
      //Add new User
      'addUser': 'Neuen User hinzufügen',
      'newUserAdded': 'Neuen User hinzugefügt',
      'minChar': 'Passwort zu kurz.',
      //add document
      'author': 'Autor',
    },
  };

  String get(String s) {
    return _localizedValues[locale.languageCode][s];
  }
}

class AppLocalizationDelegate extends LocalizationsDelegate<AppLocalizations> {
  const AppLocalizationDelegate();

  @override
  bool isSupported(Locale locale) => ['en', 'de'].contains(locale.languageCode);

  @override
  Future<AppLocalizations> load(Locale locale) {
    // Returning a SynchronousFuture here because an async "load" operation
    // isn't needed to produce an instance of DemoLocalizations.
    return SynchronousFuture<AppLocalizations>(AppLocalizations(locale));
  }

  @override
  bool shouldReload(AppLocalizationDelegate old) => false;
}
