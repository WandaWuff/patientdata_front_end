import 'package:flutter/material.dart';

class MyColors {
  static const MaterialColor PrimaryColor = MaterialColor(
    0xFF8c2f39,
    <int, Color>{
      50: Color(0xFF8c2f39),
      100: Color(0xFF8c2f39),
      200: Color(0xFF8c2f39),
      300: Color(0xFF8c2f39),
      400: Color(0xFF8c2f39),
      500: Color(0xFF8c2f39),
      600: Color(0xFF8c2f39),
      700: Color(0xFF8c2f39),
      800: Color(0xFF8c2f39),
      900: Color(0xFF8c2f39),
    },
  );

  static const PrimaryColorLight = const Color(0xFF786987);
  static const PrimaryColorExtraLight = const Color(0xFFa89db2);
  static const PrimaryDarkColor = const Color(0xFF4b2840);
  static const PrimaryAccentColor = const Color(0xFF283044);
  static const ErrorColor = const  Color(0xFF808080);
  static const White = const  Color(0xFFFFFFFF);

  static const String PrimaryColorHex = '#8c2f39';
  static const String PrimaryAccentColorHex = '#461220';
}