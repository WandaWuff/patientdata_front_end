class MySpacer {
  static const double S = 2;
  static const double M = 4;
  static const double L = 6;
  static const double XL = 10;
  static const double XXL = 12;
  static const double XXXL = 25;
}