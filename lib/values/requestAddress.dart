class RequestAddress {
  static const localIp = '192.168.1.7';

  static const main = 'http://aa155460b804d4c42b58fdbd2ea6f056-2123682587.us-east-2.elb.amazonaws.com:8080/';
  static const keyCloak = 'http://a72771d96b5f14a6d8626845bc6bf3f5-1507366330.us-east-2.elb.amazonaws.com:8080/auth/realms';

  static const register = '${RequestAddress.main}register';
  static const patient = '${RequestAddress.main}patient';
  static const users = '${RequestAddress.main}users';
  static const diagnoses = '${RequestAddress.main}diagnoses';
  static const emergencyContacts = '${RequestAddress.main}emergencyContacts';
  static const documents = '${RequestAddress.main}documents';
}