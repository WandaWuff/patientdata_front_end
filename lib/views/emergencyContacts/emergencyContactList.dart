import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:patientdata/models/emergencyContact.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/views/emergencyContacts/emergencyContactAdd.dart';
import 'package:patientdata/views/emergencyContacts/emergencyContactDetail.dart';
import 'package:patientdata/widgets/cards/emergencyContactCard.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/drawer/drawer.dart';
import 'package:patientdata/widgets/emptyList.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';

class EmergencyContactList extends StatefulWidget {
  static const String routeName = '/emergencyContact';

  @override
  EmergencyContactListState createState() => EmergencyContactListState();
}

class EmergencyContactListState extends State<EmergencyContactList> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _filterController = TextEditingController();
  String filter;
  List<EmergencyContact> emergencyContactList = new List();
  Future<List<EmergencyContact>> futureEmergencyContactList;

  bool longPressFlag = false;
  List<int> indexList = new List();

  bool sortEmergencyContacts = false;

  Future<List<EmergencyContact>> _getList() async {
    List<EmergencyContact> localList =
        await Provider.of<ApiProvider>(context, listen: false)
            .getEmergencyContactList();

    if (localList != null) {
      setState(() {
        emergencyContactList = localList;
      });
      return localList;
    } else {
      throw Exception('Failed to load');
    }
  }

  // ignore: missing_return
  Future<bool> _backPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => BaseDataOverview()));
  }

  void _unSelectAll() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => EmergencyContactList()),
    );
  }

  void longPress() {
    setState(() {
      if (indexList.isEmpty) {
        longPressFlag = false;
      } else {
        longPressFlag = true;
      }
    });
  }

  void reload() {
    setState(() {
      emergencyContactList = new List();
      _filterController.text = '';
    });
    _getList();
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  @override
  initState() {
    reload();

    futureEmergencyContactList =
        Provider.of<ApiProvider>(context, listen: false)
            .getEmergencyContactList();

    _filterController.addListener(() {
      setState(() {
        filter = _filterController.text;
      });
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _backPressed,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: MyDrawer(),
        appBar: emergencyContactList.length == 0
            ? AppBar(
                title:
                    Text(AppLocalizations.of(context).get('emergencyContacts')),
              )
            : AppBar(
                leading: indexList.length == 0
                    ? null
                    : IconButton(
                        icon: Icon(Icons.close),
                        onPressed: _unSelectAll,
                      ),
                title: indexList.length == 0
                    ? Text(
                        AppLocalizations.of(context).get('emergencyContacts'))
                    : Text('${indexList.length} ' +
                        AppLocalizations.of(context).get('selected')),
                actions: indexList.length == 0
                    ? <Widget>[
                        IconButton(
                            icon: Icon(Icons.sort_by_alpha),
                            onPressed: null //getListSorted,
                            )
                      ]
                    : <Widget>[
                        IconButton(
                            icon: Icon(Icons.delete),
                            onPressed:
                                null /*() {
                MyDialog.showDialogWithDeleteFunction(
                  context,
                  AppLocalizations.of(context)
                      .get('emergencyContactMore') +
                      ' (${indexList.length})',
                      () {
                    _delete();
                    Navigator.push(
                      context,
                        MaterialPageRoute(
                          builder: (context) => EmergencyContactList()),
                    );
                  },
                );
              },*/
                            ),
                      ],
              ),
        resizeToAvoidBottomInset: false,
        floatingActionButton: indexList.length != 0
            ? null
            : FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EmergencyContactAdd()))
                      .whenComplete(reload);
                },
                child: Icon(
                  Icons.add,
                ),
              ),
        body: DoubleBackToCloseApp(
          child: RefreshIndicator(
            onRefresh: () async {
              reload();
            },
            child: FutureBuilder<List<EmergencyContact>>(
              future: futureEmergencyContactList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length > 0) {
                    return Column(
                      children: <Widget>[
                        MyTextInput(
                          label: AppLocalizations.of(context).get('keyword'),
                          inputController: _filterController,
                        ),
                        MyDivider(EdgeInsets.symmetric(vertical: MySpacer.XL)),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, i) =>
                                filter == null || filter == ""
                                    ? EmergencyContactCard(
                                        snapshot.data[i],
                                        index: i,
                                        longPressEnabled: longPressFlag,
                                        filter: false,
                                        callback: () {
                                          if (indexList.contains(i)) {
                                            indexList.remove(i);
                                          } else {
                                            indexList.add(i);
                                          }

                                          longPress();
                                        },
                                        goTo: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    EmergencyContactDetail(
                                                        snapshot.data[i])),
                                          ).whenComplete(reload);
                                        },
                                      )
                                    : snapshot.data[i].contains(filter)
                                        ? new EmergencyContactCard(
                                            snapshot.data[i],
                                            longPressEnabled: longPressFlag,
                                            filter: true,
                                            goTo: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        EmergencyContactDetail(
                                                            snapshot.data[i])),
                                              ).whenComplete(reload);
                                            },
                                          )
                                        : new Container(),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return EmptyList(
                        AppLocalizations.of(context).get('emergencyContacts'));
                  }
                } else if (snapshot.hasError) {
                  return EmptyList(
                      AppLocalizations.of(context).get('emergencyContacts'));
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
          snackBar: SnackBar(
            content: Text(AppLocalizations.of(context).get('closeApp')),
          ),
        ),
      ),
    );
  }
}
