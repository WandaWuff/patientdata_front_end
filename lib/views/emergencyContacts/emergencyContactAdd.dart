import 'package:flutter/material.dart';
import 'package:patientdata/models/emergencyContact.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/routes/routes.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class EmergencyContactAdd extends StatefulWidget {
  @override
  _EmergencyContactAddState createState() => _EmergencyContactAddState();
}

class _EmergencyContactAddState extends State<EmergencyContactAdd> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _preNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();

  final FocusNode _preNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();

  void _saveEmergencyContact() {
    if (_preNameController.text.isEmpty ||
        _lastNameController.text.isEmpty ||
        _emailController.text.isEmpty) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedPreName = Validation.trimLastSpace(_preNameController.text);
    String validatedLastName =
        Validation.trimLastSpace(_lastNameController.text);
    String validatedEmail = Validation.trimLastSpace(_emailController.text);

    var uuid = Uuid();

    EmergencyContact emergencyContact = new EmergencyContact(
      id: uuid.v4(),
      preName: validatedPreName,
      lastName: validatedLastName,
      email: validatedEmail,
    );

    Provider.of<ApiProvider>(context, listen: false)
        .callAddEmergencyContact(emergencyContact)
        .then((value) {
      if (value == 200) {
        Navigator.pushReplacementNamed(context, Routes.emergencyView);
      }
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('addEmergencyContact')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('firstName'),
              inputController: _preNameController,
              inputAction: TextInputAction.next,
              focusNode: _preNameFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _preNameFocus, _lastNameFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('lastName'),
              inputController: _lastNameController,
              inputAction: TextInputAction.next,
              focusNode: _lastNameFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _lastNameFocus, _emailFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('email'),
              inputController: _emailController,
              inputAction: TextInputAction.next,
              focusNode: _emailFocus,
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _saveEmergencyContact,
            ),
          ],
        ),
      ),
    );
  }
}
