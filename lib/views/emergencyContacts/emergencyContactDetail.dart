import 'package:flutter/material.dart';
import 'package:patientdata/models/emergencyContact.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/views/emergencyContacts/emergencyContactEdit.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/iconElement.dart';
import 'package:patientdata/widgets/dataElement.dart';
import 'package:patientdata/widgets/divider.dart';

// ignore: must_be_immutable
class EmergencyContactDetail extends StatefulWidget {
  EmergencyContact _emergencyContact;

  EmergencyContactDetail(this._emergencyContact);

  @override
  _EmergencyContactDetailState createState() => _EmergencyContactDetailState();
}

class _EmergencyContactDetailState extends State<EmergencyContactDetail> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('emergencyContact')),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        EmergencyContactEdit(widget._emergencyContact)),
              ).then((value) {
                setState(() {
                  widget._emergencyContact = value;
                });
              });
            },
          ),
          IconButton(
              icon: Icon(Icons.delete),
              onPressed:
                  null /*() {
              MyDialog.showDialogWithDeleteFunction(
                context,
                AppLocalizations.of(context).get('deleteEmergencyContact'),
                () {
                  _delete();
                  Navigator.of(context).pop();
                },
              );
            },*/
              ),
        ],
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.top,
              columnWidths: {1: FractionColumnWidth(.8)},
              children: [
                TableRow(
                  children: [
                    DataElement(
                        element:
                            AppLocalizations.of(context).get('name') + ':'),
                    DataElement(
                        element: widget._emergencyContact.getFullName()),
                  ],
                ),
              ],
            ),
            MyDivider(EdgeInsets.all(0)),
            IconElement(
                icon: Icons.mail, value: widget._emergencyContact.email),
            MyDivider(EdgeInsets.only(bottom: MySpacer.L)),
          ],
        ),
      ),
    );
  }
}
