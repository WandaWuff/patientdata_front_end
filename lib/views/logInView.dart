import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:patientdata/models/keyCloakAccount.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:provider/provider.dart';
import '../values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/views/createUserView.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/header.dart';
import 'package:patientdata/widgets/hyperlink.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';

class LogInView extends StatefulWidget {
  static const String routeName = '/loginView';

  @override
  _LogInViewState createState() => _LogInViewState();
}

class _LogInViewState extends State<LogInView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _nameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _domainController = TextEditingController();

  final FocusNode _nameFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _domainFocus = FocusNode();

  void _logIn() {
    if (_nameController.text.isEmpty || _passwordController.text.isEmpty) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedUserName = Validation.trimLastSpace(_nameController.text);

    KeyCloakAccount kca = new KeyCloakAccount(
      clientId: "patientdata-frontend",
      username: validatedUserName,
      password: _passwordController.text,
      grantType: "password",
    );

    Provider.of<ApiProvider>(context, listen: false).callKeyCloak(kca, _domainController.text).then((value) {
      if (value == 200) {
               Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (BuildContext ctx) => BaseDataOverview()));
      } else {
        MyDialog.showDialogWithParam(
            context, AppLocalizations.of(context).get('wrongCredentials'));
      }
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: DoubleBackToCloseApp(
        child: Container(
          color: MyColors.PrimaryColor,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: MySpacer.XXXL,
                        left: MySpacer.XXXL,
                        right: MySpacer.XXXL),
                    child: Card(
                      elevation: 20,
                      color: MyColors.White,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: MyColors.PrimaryDarkColor)),
                      child: Padding(
                        padding: const EdgeInsets.all(MySpacer.XXL),
                        child: Column(
                          children: <Widget>[
                            Header(
                              AppLocalizations.of(context).get('appTitle'),
                              color: MyColors.PrimaryDarkColor,
                            ),
                            MyDivider(
                              EdgeInsets.symmetric(vertical: MySpacer.XL),
                            ),
                            MainText(
                              AppLocalizations.of(context).get('shortDesc'),
                              color: MyColors.PrimaryDarkColor,
                              align: TextAlign.center,
                            ),
                            MyDivider(
                              EdgeInsets.only(
                                  top: MySpacer.XXL, bottom: MySpacer.S),
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context).get('email'),
                              inputController: _nameController,
                              inputAction: TextInputAction.next,
                              focusNode: _nameFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _nameFocus, _passwordFocus);
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: MySpacer.S),
                            ),
                            MyTextInput(
                              label:
                                  AppLocalizations.of(context).get('password'),
                              obscureText: true,
                              inputController: _passwordController,
                              inputAction: TextInputAction.next,
                              focusNode: _passwordFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _passwordFocus, _domainFocus);
                              },
                              maxLines: 1,
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context).get('domain'),
                              inputController: _domainController,
                              inputAction: TextInputAction.done,
                              focusNode: _domainFocus,
                              maxLines: 1,
                            ),
                            MyDivider(EdgeInsets.only(top: MySpacer.L)),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: MyRaisedButton(
                                    text: AppLocalizations.of(context)
                                        .get('logIn'),
                                    function: _logIn,
                                  ),
                                ),
                              ],
                            ),
                            MyDivider(
                              EdgeInsets.only(
                                bottom: MySpacer.XL,
                              ),
                            ),
                            Hyperlink(
                              text:
                                  AppLocalizations.of(context).get('noAccount'),
                              function: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            CreateUserView()));
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        snackBar: SnackBar(
          content: Text(
            AppLocalizations.of(context).get('closeApp'),
          ),
        ),
      ),
    );
  }
}
