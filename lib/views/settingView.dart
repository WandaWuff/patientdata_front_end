import 'package:flutter/material.dart';
import 'package:patientdata/views/logInView.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/drawer/drawer.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/widgets/cards/settingCard.dart';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SettingView extends StatefulWidget {
  static const String routeName = '/settingView';

  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  void _logOff() {
    MyDialog.showDialogWithDeleteFunction(context,
        AppLocalizations.of(context).get('signOutQuestion'), setPreferences);
  }

  void setPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext ctx) => LogInView()));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: MyDrawer(),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('settings')),
      ),
      body: DoubleBackToCloseApp(
        child: Center(
          child: ListView(
            children: <Widget>[
              SettingCard(
                onTap: _logOff,
                icon: Icons.exit_to_app,
                setting: AppLocalizations.of(context).get('logOff'),
              ),
            ],
          ),
        ),
        snackBar: SnackBar(
          content: Text(AppLocalizations.of(context).get('closeApp')),
        ),
      ),
    );
  }
}
