import 'dart:io';
import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/widgets/drawer/drawer.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/emptyList.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';
import '../../widgets/cards/diagnosisCard.dart';
import 'diagnosisAdd.dart';
import 'diagnosisDetail.dart';

// ignore: must_be_immutable
class DiagnosisList extends StatefulWidget {
  static const String routeName = '/diagnosis';

  @override
  _DiagnosisListState createState() => _DiagnosisListState();
}

class _DiagnosisListState extends State<DiagnosisList> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _filterController = TextEditingController();
  String filter;
  List<Diagnosis> diagnosisList = new List();
  Future<List<Diagnosis>> futureDiagnosisList;

  bool longPressFlag = false;
  List<int> indexList = new List();

  String documentId;
  File docFile;

  List<File> documentFileList = new List();

  void longPress() {
    setState(() {
      if (indexList.isEmpty) {
        longPressFlag = false;
      } else {
        longPressFlag = true;
      }
    });
  }

  Future<List<Diagnosis>> _getList() async {
    List<Diagnosis> localList =
        await Provider.of<ApiProvider>(context, listen: false)
            .getDiagnosisList();

    if (localList != null) {
      if (this.mounted) {
        setState(() {
          diagnosisList = localList;
        });
        return localList;
      }
    } else {
      throw Exception('Failed to load');
    }
  }

  void reload() {
    setState(() {
      diagnosisList = new List();
      _filterController.text = '';
    });
    _getList();
  }

  @override
  initState() {
    super.initState();

    futureDiagnosisList =
        Provider.of<ApiProvider>(context, listen: false).getDiagnosisList();

    reload();

    _filterController.addListener(() {
      setState(() {
        filter = _filterController.text;
      });
    });
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  void _unSelectAll() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DiagnosisList()),
    );
  }

  // ignore: missing_return
  Future<bool> _backPressed() {
    if (indexList.length == 0) {
      if (_scaffoldKey.currentState.isDrawerOpen) {
        Navigator.pop(context);
      } else {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BaseDataOverview()));
      }
    } else {
      _unSelectAll();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _backPressed,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: MyDrawer(),
        appBar: diagnosisList.length == 0
            ? AppBar(
                title: Text(AppLocalizations.of(context).get('diagnosisMore')),
              )
            : AppBar(
                leading: indexList.length == 0
                    ? null
                    : IconButton(
                        icon: Icon(Icons.close),
                        onPressed: _unSelectAll,
                      ),
                title: indexList.length == 0
                    ? Text(AppLocalizations.of(context).get('diagnosisMore'))
                    : Text('${indexList.length} ' +
                        AppLocalizations.of(context).get('selected')),
                actions: indexList.length == 0
                    ? <Widget>[
                        IconButton(
                            icon: Icon(Icons.event_note),
                            onPressed: null //getListSortedByDate,
                            ),
                        IconButton(
                            icon: Icon(Icons.sort_by_alpha),
                            onPressed: null //getListSortedByDiagnosis,
                            )
                      ]
                    : <Widget>[
                        IconButton(
                            icon: Icon(Icons.delete),
                            onPressed:
                                null /*() {
                            MyDialog.showDialogWithDeleteFunction(
                              context,
                              AppLocalizations.of(context)
                                      .get('forSureDeleteMore') +
                                  ' (${indexList.length})',
                              () {
                                _delete();
                                Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) => DiagnosisList()),
                                );
                              },
                            );
                          },*/
                            ),
                      ],
              ),
        resizeToAvoidBottomInset: false,
        floatingActionButton: indexList.length != 0
            ? null
            : FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DiagnosisAdd()))
                      .whenComplete(reload);
                },
                child: Icon(
                  Icons.add,
                ),
              ),
        body: DoubleBackToCloseApp(
          child: RefreshIndicator(
            onRefresh: () async {
              reload();
            },
            child: FutureBuilder<List<Diagnosis>>(
              future: futureDiagnosisList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length > 0) {
                    return Column(
                      children: <Widget>[
                        MyTextInput(
                          label: AppLocalizations.of(context).get('keyword'),
                          inputController: _filterController,
                        ),
                        MyDivider(EdgeInsets.symmetric(vertical: MySpacer.XL)),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, i) =>
                                filter == null || filter == ""
                                    ? DiagnosisCard(
                                        snapshot.data[i],
                                        index: i,
                                        longPressEnabled: longPressFlag,
                                        filter: false,
                                        callback: () {
                                          if (indexList.contains(i)) {
                                            indexList.remove(i);
                                          } else {
                                            indexList.add(i);
                                          }

                                          longPress();
                                        },
                                        goTo: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DiagnosisDetail(
                                                        snapshot.data[i])),
                                          ).whenComplete(reload);
                                        },
                                      )
                                    : snapshot.data[i].contains(filter)
                                        ? new DiagnosisCard(
                                            snapshot.data[i],
                                            longPressEnabled: longPressFlag,
                                            filter: true,
                                            goTo: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DiagnosisDetail(
                                                          snapshot.data[i],
                                                        )),
                                              ).whenComplete(reload);
                                            },
                                          )
                                        : new Container(),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return EmptyList(
                        AppLocalizations.of(context).get('diagnosisMore'));
                  }
                } else if (snapshot.hasError) {
                  return EmptyList(
                      AppLocalizations.of(context).get('diagnosisMore'));
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
          snackBar: SnackBar(
            content: Text(AppLocalizations.of(context).get('closeApp')),
          ),
        ),
      ),
    );
  }
}
