import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/models/icd.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dateButton.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class DiagnosisEdit extends StatefulWidget {
  Diagnosis _diagnosis;

  DiagnosisEdit(this._diagnosis);

  @override
  _DiagnosisEditState createState() => _DiagnosisEditState();
}

class _DiagnosisEditState extends State<DiagnosisEdit> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _icdController = TextEditingController();
  final _placeController = TextEditingController();
  final _doctorController = TextEditingController();

  DateTime _selectedDate;
  Future<ICD> str;

  final FocusNode _icdFocus = FocusNode();
  final FocusNode _placeFocus = FocusNode();
  final FocusNode _doctorFocus = FocusNode();

  String suggestedIcd = '';

  Future<void> fetchICD(String icd, BuildContext context) async {
    String localText =
    await Provider.of<ApiProvider>(context, listen: false).fetchICD(icd);

    if (localText != null) {
      setState(() {
        suggestedIcd = localText;
      });

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: new Text(suggestedIcd),
        duration: Duration(seconds: 3),
      ));

      return localText;
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: new Text(AppLocalizations.of(context).get('failedIcd')),
        duration: Duration(seconds: 3),
      ));
      //throw Exception(AppLocalizations.of(context).get('failedIcd'));
    }
  }

  void _updateDiagnosis() async {
    if (_icdController.text.isEmpty ||
        _placeController.text.isEmpty ||
        _doctorController.text.isEmpty ||
        _selectedDate == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    if (suggestedIcd == null) {
      MyDialog.showDialogWithParam(
          context, AppLocalizations.of(context).get('failedIcd'));
      return;
    }

    String validatedICD = Validation.trimLastSpace(_icdController.text);
    String validatedPlace = Validation.trimLastSpace(_placeController.text);
    String validatedDoctor = Validation.trimLastSpace(_doctorController.text);

    Diagnosis diagnosis = new Diagnosis(
      id: widget._diagnosis.id,
      icd: validatedICD,
      date: _selectedDate,
      place: validatedPlace,
      doctor: validatedDoctor,
    );

    setState(() {
      widget._diagnosis.setDiagnosis(diagnosis);
      widget._diagnosis.setText(suggestedIcd);
    });

    Provider.of<ApiProvider>(context, listen: false)
        .callEditDiagnosis(diagnosis)
        .then((value) {
      if (value == 200) {
        Navigator.pop(context, diagnosis);
      }
    });
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: _selectedDate == null ? DateTime.now() : _selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      locale: const Locale('de', 'DE'),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  void _onFocusChange() {
    if (_icdController.text.length <= 6) {
      if (!_icdFocus.hasFocus) {
        fetchICD(_icdController.text, context);
      }
    }
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    _icdController.text = widget._diagnosis.icd;
    _placeController.text = widget._diagnosis.place;
    _doctorController.text = widget._diagnosis.doctor;
    _selectedDate = widget._diagnosis.date;

    _icdFocus.addListener(_onFocusChange);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('editDiagnosis')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('icd'),
              inputController: _icdController,
              maxChar: 6,
              inputAction: TextInputAction.done,
              focusNode: _icdFocus,
            ),
            DateButton(
              text: _selectedDate == null
                  ? DateFormat('dd.MM.yy').format(widget._diagnosis.date)
                  : DateFormat('dd.MM.yy').format(_selectedDate),
              alignment: TextAlign.left,
              function: _presentDatePicker,
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('clinic'),
              inputController: _placeController,
              inputAction: TextInputAction.done,
              focusNode: _placeFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _placeFocus, _doctorFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('physician'),
              inputController: _doctorController,
              inputAction: TextInputAction.done,
              focusNode: _doctorFocus,
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
                text: AppLocalizations.of(context).get('saveButton'),
                icon: Icons.save,
                function: _updateDiagnosis),
          ],
        ),
      ),
    );
  }
}
