import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/views/diagnosis/diagnosisAddDocument.dart';
import 'package:patientdata/widgets/cards/imageCard.dart';
import 'package:patientdata/widgets/dataElement.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:provider/provider.dart';
import 'diagnosisEdit.dart';

// ignore: must_be_immutable
class DiagnosisDetail extends StatefulWidget {
  Diagnosis _diagnosis;

  DiagnosisDetail(this._diagnosis);

  @override
  _DiagnosisDetailState createState() => _DiagnosisDetailState();
}

class _DiagnosisDetailState extends State<DiagnosisDetail> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<String> icdText;
  String suggestedIcd;
  String fetchId;

  Future<List<Document>> futureDocumentList;
  Future<List<File>> futureFileList;

  Future<String> fetchICD(String icd, BuildContext context) async {
    String localText =
        await Provider.of<ApiProvider>(context, listen: false).fetchICD(icd);

    if (localText != null) {
      return localText;
    } else {
      throw Exception(AppLocalizations.of(context).get('failedIcd'));
    }
  }

  void reload() {
    fetchICD(widget._diagnosis.icd, context);
  }

  File getFile(String id) {
    File file;
    Provider.of<ApiProvider>(context, listen: false).getFile(id).then((value) {
      setState(() {
        file = value;
      });
    });

    return file;
  }

  @override
  void initState() {
    icdText = fetchICD(widget._diagnosis.icd, context);

    futureDocumentList = Provider.of<ApiProvider>(context, listen: false)
        .getDocumentListFromDiagnosis(widget._diagnosis.id)
        .then((value) {
          setState(() {
            futureFileList =
                Provider.of<ApiProvider>(context, listen: false).getFiles(value);
          });


      return value;
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String formattedDateDiagnosed;
    if (widget._diagnosis != null) {
      formattedDateDiagnosed =
          DateFormat('dd.MM.yy').format(widget._diagnosis.date);
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('detail')),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DiagnosisEdit(widget._diagnosis)),
              ).then((value) {
                setState(() {
                  widget._diagnosis = value;
                });
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.add_a_photo),
            onPressed: () {
              //TODO: reload widget after document added
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        DiagnosisAddDocument(diagnosis: widget._diagnosis)),
              );
            },
          ),
        ],
      ),
      body: Container(
        child: FutureBuilder<String>(
          future: fetchICD(widget._diagnosis.icd, context),
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              return ListView(
                children: <Widget>[
                  Table(
                    defaultVerticalAlignment: TableCellVerticalAlignment.top,
                    columnWidths: {1: FractionColumnWidth(.5)},
                    children: [
                      TableRow(
                        children: [
                          DataElement(
                              element: AppLocalizations.of(context).get('icd') +
                                  ':'),
                          DataElement(element: widget._diagnosis.icd),
                        ],
                      ),
                      TableRow(
                        children: [
                          DataElement(
                              element: AppLocalizations.of(context)
                                      .get('diagnosis') +
                                  ':'),
                          DataElement(element: snapshot.data),
                        ],
                      ),
                      TableRow(
                        children: [
                          DataElement(
                              element: AppLocalizations.of(context)
                                      .get('diagnosedOn') +
                                  ':'),
                          DataElement(element: formattedDateDiagnosed),
                        ],
                      ),
                      TableRow(
                        children: [
                          DataElement(
                              element:
                                  AppLocalizations.of(context).get('clinic') +
                                      ':'),
                          DataElement(element: widget._diagnosis.place),
                        ],
                      ),
                      TableRow(
                        children: [
                          DataElement(
                              element: AppLocalizations.of(context)
                                      .get('physician') +
                                  ':'),
                          DataElement(element: widget._diagnosis.doctor),
                        ],
                      ),
                    ],
                  ),
                  MyDivider(EdgeInsets.only(bottom: MySpacer.L)),
                  FutureBuilder<List<File>>(
                    future: futureFileList,
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        return ListView.builder(
                            physics: ClampingScrollPhysics(),
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: snapshot.data.length,
                            itemBuilder: (context, i) {
                              return ImageCard(snapshot.data[i]);
                            });
                      }
                      return Center(child: CircularProgressIndicator());
                    },
                  ),
                  MyDivider(EdgeInsets.only(top: MySpacer.L)),
                ],
              );
            }

            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
