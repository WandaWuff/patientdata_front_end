import 'dart:io';

import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/models/icd.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/routes/routes.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dateButton.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/imageInput.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:path/path.dart' as path;
import 'package:path_provider/path_provider.dart' as sysPaths;
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

class DiagnosisAdd extends StatefulWidget {
  @override
  _DiagnosisAddState createState() => _DiagnosisAddState();
}

class _DiagnosisAddState extends State<DiagnosisAdd> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _icdController = TextEditingController();
  final _placeController = TextEditingController();
  final _doctorController = TextEditingController();

  DateTime _selectedDate;
  Future<ICD> str;

  final FocusNode _icdFocus = FocusNode();
  final FocusNode _placeFocus = FocusNode();
  final FocusNode _doctorFocus = FocusNode();

  String suggestedIcd;
  File _storedImage;

  Future<void> fetchICD(String icd, BuildContext context) async {
    String localText =
        await Provider.of<ApiProvider>(context, listen: false).fetchICD(icd);

    if (localText != null) {
      setState(() {
        suggestedIcd = localText;
      });

      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: new Text(suggestedIcd),
        duration: Duration(seconds: 3),
      ));

      return localText;
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content: new Text(AppLocalizations.of(context).get('failedIcd')),
        duration: Duration(seconds: 3),
      ));
      //throw Exception(AppLocalizations.of(context).get('failedIcd'));
    }
  }

  void _selectImage(File pickedImage) {
    setState(() {
      _storedImage = pickedImage;
    });
  }

  void _saveDiagnosis() {
    if (_icdController.text.isEmpty ||
        _placeController.text.isEmpty ||
        _doctorController.text.isEmpty ||
        _selectedDate == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    if (suggestedIcd == null) {
      MyDialog.showDialogWithParam(
          context, AppLocalizations.of(context).get('noIcd'));
      return;
    }

    String validatedICD = Validation.trimLastSpace(_icdController.text);
    String validatedPlace = Validation.trimLastSpace(_placeController.text);
    String validatedDoctor = Validation.trimLastSpace(_doctorController.text);

    var uuid = Uuid().v4();

    Diagnosis diagnosis = new Diagnosis(
      id: uuid,
      icd: validatedICD,
      date: _selectedDate,
      place: validatedPlace,
      doctor: validatedDoctor,
    );

    Provider.of<ApiProvider>(context, listen: false)
        .callAddDiagnosis(diagnosis)
        .then((value) {
      if (value == 200) {
        if (_storedImage != null) {
          var docUuid = Uuid().v4();

          Document document = new Document(
            id: docUuid,
            title: validatedICD,
            author: validatedDoctor,
            description: validatedPlace,
            date: _selectedDate,
          );

          Provider.of<ApiProvider>(context, listen: false)
              .callPostFileToDiagnosis(uuid, document)
              .then((value) {
            if (value == 200) {
              Provider.of<ApiProvider>(context, listen: false)
                  .callPostFile(docUuid, _storedImage)
                  .then((value) {
                if (value == 200) {
                  Navigator.pushReplacementNamed(context, Routes.diagnosis);
                } else {
                  _scaffoldKey.currentState.showSnackBar(SnackBar(
                    content:
                        new Text(AppLocalizations.of(context).get('failed')),
                    duration: Duration(seconds: 3),
                  ));
                }
              });
            } else {
              _scaffoldKey.currentState.showSnackBar(SnackBar(
                content: new Text(AppLocalizations.of(context).get('failed')),
                duration: Duration(seconds: 3),
              ));
            }
          });
        } else {
          Navigator.pushReplacementNamed(context, Routes.diagnosis);
        }
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: new Text(AppLocalizations.of(context).get('failed')),
          duration: Duration(seconds: 3),
        ));
      }
    });
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: _selectedDate == null ? DateTime.now() : _selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      locale: const Locale('de', 'DE'),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  final picker = ImagePicker();

  Future<void> _takePicture() async {
    final imageFile = await picker.getImage(
      source: ImageSource.gallery,
      maxWidth: 600,
    );
    if (imageFile == null) {
      return;
    }
    setState(() {
      _storedImage = File(imageFile.path);
    });
    final appDir = await sysPaths.getApplicationDocumentsDirectory();
    final fileName = path.basename(imageFile.path);
    final savedImage =
        await File(imageFile.path).copy('${appDir.path}/$fileName');

    setState(() {
      _storedImage = savedImage;
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  void _onFocusChange() {
    if (_icdController.text.length <= 6) {
      if (!_icdFocus.hasFocus) {
        fetchICD(_icdController.text, context);
      }
    }
  }

  @override
  void initState() {
    _icdFocus.addListener(_onFocusChange);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('addDiagnosis')),
        leading: IconButton(
          icon: Icon(Icons.arrow_back),
          onPressed: () => Navigator.pop(context, false),
        ),
        actions: _storedImage == null
            ? <Widget>[
                IconButton(
                  icon: Icon(Icons.add_a_photo),
                  onPressed: _takePicture,
                ),
              ]
            : null,
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('icd'),
              inputController: _icdController,
              maxChar: 6,
              inputAction: TextInputAction.done,
              focusNode: _icdFocus,
            ),
            DateButton(
              text: _selectedDate == null
                  ? AppLocalizations.of(context).get('diagnosedOn')
                  : DateFormat('dd.MM.yy').format(_selectedDate),
              alignment: TextAlign.left,
              function: _presentDatePicker,
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('clinic'),
              inputController: _placeController,
              inputAction: TextInputAction.next,
              focusNode: _placeFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _placeFocus, _doctorFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('physician'),
              inputController: _doctorController,
              inputAction: TextInputAction.done,
              focusNode: _doctorFocus,
            ),
            _storedImage == null
                ? Container()
                : ImageInput(_selectImage,
                    AppLocalizations.of(context).get('changePicture'),
                    file: _storedImage),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _saveDiagnosis,
            ),
          ],
        ),
      ),
    );
  }
}
