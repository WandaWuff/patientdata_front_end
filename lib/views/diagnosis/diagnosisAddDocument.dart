import 'dart:io';

import 'package:flutter/material.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dataElement.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/imageInput.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';

// ignore: must_be_immutable
class DiagnosisAddDocument extends StatefulWidget {
  final Diagnosis diagnosis;

  DiagnosisAddDocument({Key key, this.diagnosis});

  @override
  _DiagnosisAddDocumentState createState() => _DiagnosisAddDocumentState();
}

class _DiagnosisAddDocumentState extends State<DiagnosisAddDocument> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  File file;

  void _selectImage(File pickedImage) {
    setState(() {
      file = pickedImage;
    });
  }

  void _addDocument() {
    if (file == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    var docUuid = Uuid().v4();

    Document document = new Document(
      id: docUuid,
      title: widget.diagnosis.icd,
      author: widget.diagnosis.doctor,
      description: widget.diagnosis.place,
      date: widget.diagnosis.date,
    );

    Provider.of<ApiProvider>(context, listen: false)
        .callPostFileToDiagnosis(widget.diagnosis.id, document)
        .then((value) {
      if (value == 200) {
        Provider.of<ApiProvider>(context, listen: false)
            .callPostFile(docUuid, file)
            .then((value) {
          if (value == 200) {
            Navigator.pop(context, widget.diagnosis);
          } else {
            _scaffoldKey.currentState.showSnackBar(SnackBar(
              content: new Text(AppLocalizations.of(context).get('failed')),
              duration: Duration(seconds: 3),
            ));
          }
        });
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: new Text(AppLocalizations.of(context).get('failed')),
          duration: Duration(seconds: 3),
        ));
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('addDocument')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        child: ListView(
          children: <Widget>[
            Table(
              defaultVerticalAlignment: TableCellVerticalAlignment.top,
              columnWidths: {1: FractionColumnWidth(.5)},
              children: [
                TableRow(
                  children: [
                    DataElement(
                        element: AppLocalizations.of(context).get('icd') + ':'),
                    DataElement(element: widget.diagnosis.icd),
                  ],
                ),
              ],
            ),
            ImageInput(
              _selectImage,
              file == null
                  ? AppLocalizations.of(context).get('takePicture')
                  : AppLocalizations.of(context).get('changePicture'),
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _addDocument,
            )
          ],
        ),
      ),
    );
  }
}
