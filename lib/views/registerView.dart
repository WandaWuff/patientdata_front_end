import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/keyCloakAccount.dart';
import 'package:patientdata/models/patient.dart';
import 'package:patientdata/models/registration.dart';
import 'package:patientdata/models/tenant.dart';
import 'package:patientdata/models/user.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/widgets/dataElement.dart';
import 'package:patientdata/widgets/dateButton.dart';
import 'package:patientdata/widgets/datePicker.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/header.dart';
import 'package:patientdata/widgets/radioGroup.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:patientdata/widgets/textInput.dart';

class RegisterView extends StatefulWidget {
  final User user;

  RegisterView({this.user});

  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //TextEditing Controllers
  final _genderController = TextEditingController();
  final _bloodFormulaController = TextEditingController();
  final _cityOfBirthController = TextEditingController();
  final _insuranceController = TextEditingController();

  //Textediting focus
  final FocusNode _birthCityFocus = FocusNode();
  final FocusNode _bloodFormulaFocus = FocusNode();
  final FocusNode _insuranceFocus = FocusNode();

  DateTime _selectedDate;
  String accessToken;

  void _showDatePicker() {
    DatePicker.presentDatePicker(
      context,
      _selectedDate == null ? DateTime.now() : _selectedDate,
      DateTime(1900),
      DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  int _radioValue = -1;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _genderController.text = 'F';
          break;
        case 1:
          _genderController.text = 'M';
          break;
        case 2:
          _genderController.text = 'X';
          break;
      }
    });
  }

  void _register() {
    if (_genderController.text.isEmpty ||
        _bloodFormulaController.text.isEmpty ||
        _cityOfBirthController.text.isEmpty ||
        _insuranceController.text.isEmpty ||
        _selectedDate == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedBlood =
        Validation.trimLastSpace(_bloodFormulaController.text);
    String validatedCOB = Validation.trimLastSpace(_cityOfBirthController.text);
    String validatedInsurance =
        Validation.trimLastSpace(_insuranceController.text);

    Tenant tenant = new Tenant(domain: widget.user.email);

    var uuid = Uuid();

    Patient patient = new Patient(
      id: uuid.v4(),
      prename: widget.user.firstName,
      lastname: widget.user.lastName,
      email: widget.user.email,
      sex: _genderController.text,
      dateOfBirth: _selectedDate,
      placeOfBirth: validatedCOB,
      healthInsurance: validatedInsurance,
      bloodFormula: validatedBlood,
    );

    Registration registration = new Registration(
      tenant: tenant,
      user: widget.user,
      patient: patient,
    );

    Provider.of<ApiProvider>(context, listen: false)
        .callRegister(registration)
        .then((value) {
      if (value == 201) {
        KeyCloakAccount kca = new KeyCloakAccount(
          clientId: "patientdata-frontend",
          username: widget.user.email,
          password: widget.user.password,
          grantType: "password",
        );

        Provider.of<ApiProvider>(context, listen: false)
            .callKeyCloakAndReturnToken(kca)
            .then((value) {
          setState(() {
            accessToken = value;
          });

          _getPreferences(kca.username, kca.password);
        });
      } else {
        MyDialog.showDialogWithParam(context, AppLocalizations.of(context).get('failed'));
      }
    });
  }

  void _getPreferences(String username, String password) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString('status', 'loggedIn');
    prefs.setString('accessToken', accessToken);
    prefs.setString('username', username);
    prefs.setString('password', password);
    prefs.setString('tenant', username);

    Navigator.pushReplacement(context,
        MaterialPageRoute(builder: (BuildContext ctx) => BaseDataOverview()));
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: DoubleBackToCloseApp(
        child: Container(
          color: MyColors.PrimaryColor,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: MySpacer.XXXL,
                        left: MySpacer.XXXL,
                        right: MySpacer.XXXL),
                    child: Card(
                      elevation: 20,
                      color: MyColors.White,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: MyColors.PrimaryDarkColor)),
                      child: Padding(
                        padding: const EdgeInsets.all(MySpacer.XXL),
                        child: Column(
                          children: <Widget>[
                            Header(
                              AppLocalizations.of(context).get('createAccount'),
                              color: MyColors.PrimaryDarkColor,
                            ),
                            MyDivider(
                              EdgeInsets.only(top: MySpacer.XL),
                            ),
                            DataElement(
                              element: AppLocalizations.of(context).get('name'),
                              value: widget.user.getFullName(),
                            ),
                            DataElement(
                              element:
                                  AppLocalizations.of(context).get('email'),
                              value: widget.user.email,
                            ),
                            MyDivider(
                              EdgeInsets.only(bottom: MySpacer.S),
                            ),
                            RadioGroup(
                              groupValue: _radioValue,
                              onChanged: _handleRadioValueChange,
                            ),
                            MyDivider(
                              EdgeInsets.symmetric(vertical: MySpacer.S),
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context)
                                  .get('bloodFormula'),
                              inputController: _bloodFormulaController,
                              inputAction: TextInputAction.done,
                              focusNode: _bloodFormulaFocus,
                            ),
                            DateButton(
                              text: _selectedDate == null
                                  ? AppLocalizations.of(context).get('dob')
                                  : DateFormat('dd.MM.yy')
                                      .format(_selectedDate),
                              alignment: TextAlign.left,
                              function: _showDatePicker,
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context).get('cob'),
                              inputController: _cityOfBirthController,
                              inputAction: TextInputAction.next,
                              focusNode: _birthCityFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _birthCityFocus, _insuranceFocus);
                              },
                            ),
                            MyTextInput(
                              label:
                                  AppLocalizations.of(context).get('insurance'),
                              inputController: _insuranceController,
                              inputAction: TextInputAction.done,
                              focusNode: _insuranceFocus,
                            ),
                            MyDivider(EdgeInsets.only(
                                top: MySpacer.XL, bottom: MySpacer.S)),
                            MyRaisedButton(
                              text: AppLocalizations.of(context)
                                  .get('saveButton'),
                              icon: Icons.save,
                              function: _register,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
        snackBar: SnackBar(
          content: Text(AppLocalizations.of(context).get('closeApp')),
        ),
      ),
    );
  }
}
