import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/patient.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/views/logInView.dart';
import 'package:patientdata/views/newUser/newUserAdd.dart';
import 'package:patientdata/widgets/drawer/drawer.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/dataElement.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'baseDataEdit.dart';

// ignore: must_be_immutable
class BaseDataOverview extends StatefulWidget {
  static const String routeName = '/baseData';

  @override
  _BaseDataOverviewState createState() => _BaseDataOverviewState();
}

class _BaseDataOverviewState extends State<BaseDataOverview> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Future<Patient> futurePatient;
  Patient patient;
  bool isAdmin;

  Future<Patient> _getPatient() async {
    Patient pat = await Provider.of<ApiProvider>(context, listen: false)
        .getPatientAndSetFullName();

    if (pat != null) {
      setState(() {
        patient = pat;
      });
    } else {
      _scaffoldKey.currentState.showSnackBar(SnackBar(
        content:
            new Text(/*AppLocalizations.of(context).get('apiNotReachable')*/
                'Access token invalid'),
        duration: Duration(seconds: 3),
      ));
    }

    return pat;
  }

  void setPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.clear();

    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext ctx) => LogInView()));
  }

  String _getGender(String s) {
    String gender;
    switch (s) {
      case 'F':
        gender = AppLocalizations.of(context).get('female');
        break;
      case 'M':
        gender = AppLocalizations.of(context).get('male');
        break;
      case 'X':
        gender = AppLocalizations.of(context).get('divers');
        break;
    }
    return gender;
  }

  // ignore: missing_return
  Future<bool> _backPressed() {
    if (_scaffoldKey.currentState.isDrawerOpen) {
      Navigator.pop(context);
    } else {
      SystemNavigator.pop();
    }
  }

  Future<void> checkIfAdmin() async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String domain = pf.getString('username');
    String tenant = pf.getString('tenant');

    if (domain == tenant) {
      isAdmin = true;
    } else {
      isAdmin = false;
    }
  }

  void _addNewUser() async {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => NewUserAdd()),
    ).then((value) {
      if (value) {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: new Text(AppLocalizations.of(context).get('newUserAdded')),
          duration: Duration(seconds: 3),
        ));
      }
    });
  }

  @override
  void initState() {
    super.initState();
    checkIfAdmin();
    futurePatient = Provider.of<ApiProvider>(context, listen: false)
        .getPatientAndSetFullName()
        .then((value) {
      setState(() {
        patient = value;
      });
      return value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _backPressed,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: MyDrawer(),
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('basedata')),
          actions: <Widget>[
            patient == null
                ? Container()
                : IconButton(
                    icon: Icon(Icons.edit),
                    onPressed: () {
                      Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BaseDataEdit(patient)))
                          .then((value) {
                        setState(() {
                          patient = value;
                        });
                      });
                    },
                  ),
          ],
        ),
        body: DoubleBackToCloseApp(
          child: RefreshIndicator(
            onRefresh: () async {
              _getPatient();
            },
            child: FutureBuilder<Patient>(
              future: futurePatient,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  return ListView(
                    children: <Widget>[
                      Table(
                        defaultVerticalAlignment:
                            TableCellVerticalAlignment.top,
                        columnWidths: {1: FractionColumnWidth(.7)},
                        children: [
                          TableRow(
                            children: [
                              DataElement(
                                  element:
                                      AppLocalizations.of(context).get('name') +
                                          ':'),
                              DataElement(element: snapshot.data.getFullName()),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataElement(
                                  element: AppLocalizations.of(context)
                                          .get('gender') +
                                      ':'),
                              DataElement(
                                  element: _getGender(snapshot.data.sex)),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataElement(
                                  element: AppLocalizations.of(context)
                                          .get('bloodFormula') +
                                      ':'),
                              DataElement(element: snapshot.data.bloodFormula),
                            ],
                          ),
                          TableRow(
                            children: [
                              DataElement(
                                  element:
                                      AppLocalizations.of(context).get('dob') +
                                          ':'),
                              DataElement(
                                element: DateFormat('dd.MM.yy')
                                        .format(snapshot.data.dateOfBirth) +
                                    '\n' +
                                    AppLocalizations.of(context).get('in') +
                                    ' ' +
                                    snapshot.data.placeOfBirth,
                              ),
                            ],
                          ),
                        ],
                      ),
                      DataElement(
                          element:
                              AppLocalizations.of(context).get('insurance'),
                          value: snapshot.data.healthInsurance),
                      MyDivider(EdgeInsets.all(0)),
                      DataElement(
                          element: AppLocalizations.of(context).get('email'),
                          value: snapshot.data.email),
                      MyDivider(EdgeInsets.all(0)),
                      isAdmin
                          ? MyRaisedButton(
                              text: AppLocalizations.of(context)
                                  .get('addUserButton'),
                              icon: Icons.person_add,
                              function: _addNewUser,
                            )
                          : Container(),
                    ],
                  );
                }

                if (snapshot.hasError) {
                  setPreferences();
                }

                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
          snackBar: SnackBar(
            content: Text(AppLocalizations.of(context).get('closeApp')),
          ),
        ),
      ),
    );
  }
}
