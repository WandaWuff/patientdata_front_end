import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/patient.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dateButton.dart';
import 'package:patientdata/widgets/datePicker.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/radioGroup.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';

// ignore: must_be_immutable
class BaseDataEdit extends StatefulWidget {
  Patient patient;

  BaseDataEdit(this.patient);

  @override
  _BaseDataEditState createState() => _BaseDataEditState();
}

class _BaseDataEditState extends State<BaseDataEdit> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  //TextEditing Controllers
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _sexController = TextEditingController();
  final _placeOfBirthController = TextEditingController();
  final _insuranceController = TextEditingController();
  final _bloodFormulaController = TextEditingController();

  //Textediting focus
  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _placeOfBirthFocus = FocusNode();
  final FocusNode _insuranceFocus = FocusNode();
  final FocusNode _bloodFormulaFocus = FocusNode();

  DateTime _selectedDate;

  void _showDatePicker() {
    DatePicker.presentDatePicker(
      context,
      _selectedDate == null ? DateTime.now() : _selectedDate,
      DateTime(1900),
      DateTime.now(),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  void _editPatient() async {
    if (_selectedDate == null ||
        _firstNameController.text.isEmpty ||
        _lastNameController.text.isEmpty ||
        _placeOfBirthController.text.isEmpty ||
        _insuranceController.text.isEmpty ||
        _sexController.text.isEmpty ||
        _bloodFormulaController.text.isEmpty) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedFirstName =
        Validation.trimLastSpace(_firstNameController.text);
    String validatedLastName =
        Validation.trimLastSpace(_lastNameController.text);
    String validatedPlaceOfBirth =
        Validation.trimLastSpace(_placeOfBirthController.text);
    String validatedInsurance =
        Validation.trimLastSpace(_insuranceController.text);
    String validatedBloodFormula =
        Validation.trimLastSpace(_bloodFormulaController.text);

    Patient editedPatient = new Patient(
      id: widget.patient.id,
      prename: validatedFirstName,
      lastname: validatedLastName,
      email: widget.patient.email,
      sex: _sexController.text,
      dateOfBirth: _selectedDate,
      placeOfBirth: validatedPlaceOfBirth,
      healthInsurance: validatedInsurance,
      bloodFormula: validatedBloodFormula,
    );

    setState(() {
      widget.patient.setPatient(editedPatient);
    });

    int statusCode = await Provider.of<ApiProvider>(context, listen: false)
        .callEditPatient(editedPatient);
    if (statusCode == 200) {
      Navigator.pop(context, editedPatient);
    } else {
      SnackBar(
        content: Text(AppLocalizations.of(context).get('editingFailed')),
      );
    }
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  int _radioValue = -1;

  void _handleRadioValueChange(int value) {
    setState(() {
      _radioValue = value;

      switch (_radioValue) {
        case 0:
          _sexController.text = 'F';
          break;
        case 1:
          _sexController.text = 'M';
          break;
        case 2:
          _sexController.text = 'X';
          break;
      }
    });
  }

  @override
  void initState() {
      _firstNameController.text = widget.patient.prename;
      _lastNameController.text = widget.patient.lastname;
      _selectedDate = widget.patient.dateOfBirth;
      _placeOfBirthController.text = widget.patient.placeOfBirth;
      _insuranceController.text = widget.patient.healthInsurance;
      _bloodFormulaController.text = widget.patient.bloodFormula;

      if (widget.patient.sex == 'F') {
        _radioValue = 0;
        _sexController.text = 'F';
      } else if (widget.patient.sex == 'M') {
        _radioValue = 1;
        _sexController.text = 'M';
      } else {
        _radioValue = 2;
        _sexController.text = 'X';
      }


    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('basedataEdit')),
        ),
        body: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('firstName'),
              inputController: _firstNameController,
              inputAction: TextInputAction.next,
              focusNode: _firstNameFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('lastName'),
              inputController: _lastNameController,
              inputAction: TextInputAction.done,
              focusNode: _lastNameFocus,
            ),
            RadioGroup(
              groupValue: _radioValue,
              onChanged: _handleRadioValueChange,
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('bloodFormula'),
              inputController: _bloodFormulaController,
              inputAction: TextInputAction.done,
              focusNode: _bloodFormulaFocus,
            ),
            DateButton(
              text: _selectedDate == null
                  ? AppLocalizations.of(context).get('dob')
                  : DateFormat('dd.MM.yy').format(_selectedDate),
              alignment: TextAlign.left,
              function: _showDatePicker,
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('cob'),
              inputController: _placeOfBirthController,
              inputAction: TextInputAction.next,
              focusNode: _placeOfBirthFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _placeOfBirthFocus, _insuranceFocus);
              },
            ),
            MyDivider(EdgeInsets.symmetric(vertical: MySpacer.XL)),
            MyTextInput(
              label: AppLocalizations.of(context).get('insurance'),
              inputController: _insuranceController,
              inputAction: TextInputAction.done,
              focusNode: _insuranceFocus,
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _editPatient,
            ),
          ],
        ));
  }
}
