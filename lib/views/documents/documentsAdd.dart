import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/routes/routes.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:provider/provider.dart';
import 'dart:io';
import '../../widgets/imageInput.dart';
import '../../widgets/dateButton.dart';
import '../../widgets/divider.dart';
import '../../widgets/raisedButton.dart';
import '../../widgets/textInput.dart';
import 'package:uuid/uuid.dart';

class DocumentsAdd extends StatefulWidget {
  @override
  _DocumentsAddState createState() => _DocumentsAddState();
}

class _DocumentsAddState extends State<DocumentsAdd> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _descriptionController = TextEditingController();

  DateTime _selectedDate;

  final FocusNode _titleFocus = FocusNode();
  final FocusNode _authorFocus = FocusNode();
  final FocusNode _descriptionFocus = FocusNode();

  File _firstImage;

  void _selectImage(File pickedImage) {
    setState(() {
      _firstImage = pickedImage;
    });
  }

  void _saveDocument() {
    if (_titleController.text.isEmpty ||
        _authorController.text.isEmpty ||
        _descriptionController.text.isEmpty ||
        _selectedDate == null ||
        _firstImage == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedTitle = Validation.trimLastSpace(_titleController.text);
    String validatedAuthor = Validation.trimLastSpace(_authorController.text);
    String validatedDescription =
        Validation.trimLastSpace(_descriptionController.text);

    var uuid = Uuid().v4();

    Document document = new Document(
      id: uuid,
      title: validatedTitle,
      author: validatedAuthor,
      description: validatedDescription,
      date: _selectedDate,
    );

    //TODO: Add loading screen when pressed on button
    Provider.of<ApiProvider>(context, listen: false)
        .callAddDocument(document)
        .then((value) {
      if (value == 200) {
        print(value);
        Provider.of<ApiProvider>(context, listen: false)
            .callPostFile(uuid, _firstImage)
            .then((value) {
              print(value);
          if (value == 200) {
            Navigator.pushReplacementNamed(context, Routes.documents);
          }
        });
      }
    });
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: _selectedDate == null ? DateTime.now() : _selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      locale: const Locale('de', 'DE'),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('addDocument')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('title'),
              inputController: _titleController,
              inputAction: TextInputAction.next,
              focusNode: _titleFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _titleFocus, _authorFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('author'),
              inputController: _authorController,
              inputAction: TextInputAction.next,
              focusNode: _authorFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _authorFocus, _descriptionFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('description'),
              inputController: _descriptionController,
              inputAction: TextInputAction.done,
              focusNode: _descriptionFocus,
            ),
            DateButton(
              text: _selectedDate == null
                  ? AppLocalizations.of(context).get('documentDate')
                  : DateFormat('dd.MM.yy').format(_selectedDate),
              alignment: TextAlign.left,
              function: _presentDatePicker,
            ),
            MyDivider(
              EdgeInsets.only(
                top: MySpacer.XL,
              ),
            ),
            SizedBox(
              height: 10,
            ),
            ImageInput(
                _selectImage,
                _firstImage == null
                    ? AppLocalizations.of(context).get('takePicture')
                    : AppLocalizations.of(context).get('changePicture')),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _saveDocument,
            ),
          ],
        ),
      ),
    );
  }
}
