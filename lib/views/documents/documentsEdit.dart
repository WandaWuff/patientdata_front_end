import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dateButton.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';

class DocumentsEdit extends StatefulWidget {
  final Document _document;

  DocumentsEdit(this._document);

  @override
  _DocumentsEditState createState() => _DocumentsEditState();
}

class _DocumentsEditState extends State<DocumentsEdit> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _titleController = TextEditingController();
  final _authorController = TextEditingController();
  final _descriptionController = TextEditingController();

  DateTime _selectedDate;

  final FocusNode _titleFocus = FocusNode();
  final FocusNode _authorFocus = FocusNode();
  final FocusNode _descriptionFocus = FocusNode();

  void _updateDocument() async {
    if (_titleController.text.isEmpty ||
        _authorController.text.isEmpty ||
        _descriptionController.text.isEmpty ||
        _selectedDate == null) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedTitle = Validation.trimLastSpace(_titleController.text);
    String validatedAuthor = Validation.trimLastSpace(_authorController.text);
    String validatedDescription =
        Validation.trimLastSpace(_descriptionController.text);

    Document document = new Document(
      id: widget._document.id,
      title: validatedTitle,
      author: validatedAuthor,
      description: validatedDescription,
      date: _selectedDate,
    );

    setState(() {
      widget._document.setDocument(document);
    });

    Provider.of<ApiProvider>(context, listen: false)
        .callEditDocument(document)
        .then((value) {
      if (value == 200) {
        Navigator.pop(context, document);
      }
    });
  }

  void _presentDatePicker() {
    showDatePicker(
      context: context,
      initialDate: _selectedDate == null ? DateTime.now() : _selectedDate,
      firstDate: DateTime(1900),
      lastDate: DateTime.now(),
      locale: const Locale('de', 'DE'),
    ).then((pickedDate) {
      if (pickedDate == null) {
        return;
      }
      setState(() {
        _selectedDate = pickedDate;
      });
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    _titleController.text = widget._document.title;
    _authorController.text = widget._document.author;
    _descriptionController.text = widget._document.description;
    _selectedDate = widget._document.date;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
          automaticallyImplyLeading: true,
          title: Text(AppLocalizations.of(context).get('editDocument')),
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            onPressed: () => Navigator.pop(context, false),
          )),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('title'),
              inputController: _titleController,
              inputAction: TextInputAction.next,
              focusNode: _titleFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _titleFocus, _authorFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('author'),
              inputController: _authorController,
              inputAction: TextInputAction.next,
              focusNode: _authorFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _authorFocus, _descriptionFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('description'),
              inputController: _descriptionController,
              inputAction: TextInputAction.done,
              focusNode: _descriptionFocus,
            ),
            DateButton(
              text: _selectedDate == null
                  ? DateFormat('dd.MM.yy').format(widget._document.date)
                  : DateFormat('dd.MM.yy').format(_selectedDate),
              alignment: TextAlign.left,
              function: _presentDatePicker,
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _updateDocument,
            ),
          ],
        ),
      ),
    );
  }
}
