import 'package:double_back_to_close_app/double_back_to_close_app.dart';
import 'package:flutter/material.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/views/baseData/baseDataOverview.dart';
import 'package:patientdata/widgets/drawer/drawer.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/emptyList.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';
import '../../widgets/cards/documentCard.dart';
import 'documentsAdd.dart';
import 'documentsDetail.dart';

class DocumentsList extends StatefulWidget {
  static const String routeName = '/documents';

  @override
  _DocumentsListState createState() => _DocumentsListState();
}

class _DocumentsListState extends State<DocumentsList> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _filterController = TextEditingController();
  String filter;
  List<Document> documentsList = new List();
  Future<List<Document>> futureDocumentList;

  bool longPressFlag = false;
  List<int> indexList = new List();

  void longPress() {
    setState(() {
      if (indexList.isEmpty) {
        longPressFlag = false;
      } else {
        longPressFlag = true;
      }
    });
  }

  Future<List<Document>> _getList() async {
    List<Document> localList =
        await Provider.of<ApiProvider>(context, listen: false)
            .getDocumentList();

    if (localList != null) {
      setState(() {
        documentsList = localList;
      });
      return localList;
    } else {
      throw Exception('Failed to load');
    }
  }

  void reload() {
    setState(() {
      documentsList = new List();
      _filterController.text = '';
    });
    _getList();
  }

  @override
  initState() {
    super.initState();

    futureDocumentList =
        Provider.of<ApiProvider>(context, listen: false).getDocumentList();

    reload();

    _filterController.addListener(() {
      setState(() {
        filter = _filterController.text;
      });
    });
  }

  @override
  void dispose() {
    _filterController.dispose();
    super.dispose();
  }

  void _unSelectAll() {
    Navigator.push(
      context,
      MaterialPageRoute(builder: (context) => DocumentsList()),
    );
  }

  // ignore: missing_return
  Future<bool> _backPressed() {
    if (indexList.length == 0) {
      if (_scaffoldKey.currentState.isDrawerOpen) {
        Navigator.pop(context);
      } else {
        Navigator.push(context,
            MaterialPageRoute(builder: (context) => BaseDataOverview()));
      }
    } else {
      _unSelectAll();
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _backPressed,
      child: Scaffold(
        key: _scaffoldKey,
        drawer: MyDrawer(),
        appBar: documentsList.length == 0
            ? AppBar(
                title: Text(AppLocalizations.of(context).get('documents')),
              )
            : AppBar(
                leading: indexList.length == 0
                    ? null
                    : IconButton(
                        icon: Icon(Icons.close),
                        onPressed: _unSelectAll,
                      ),
                title: indexList.length == 0
                    ? Text(AppLocalizations.of(context).get('documents'))
                    : Text('${indexList.length} ' +
                        AppLocalizations.of(context).get('selected')),
                actions: indexList.length == 0
                    ? <Widget>[
                        IconButton(
                          icon: Icon(Icons.event_note),
                          onPressed: null,
                        ),
                        IconButton(
                          icon: Icon(Icons.sort_by_alpha),
                          onPressed: null,
                        )
                      ]
                    : <Widget>[
                        IconButton(
                            icon: Icon(Icons.delete),
                            onPressed:
                                null /*() {
                              MyDialog.showDialogWithDeleteFunction(
                                context,
                                AppLocalizations.of(context)
                                        .get('deleteDocuments') +
                                    ' (${indexList.length})',
                                () {
                                  _delete();
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => DocumentsList()),
                                  );
                                },
                              );
                            },*/
                            ),
                      ],
              ),
        resizeToAvoidBottomInset: false,
        floatingActionButton: indexList.length != 0
            ? null
            : FloatingActionButton(
                onPressed: () {
                  Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => DocumentsAdd()))
                      .whenComplete(reload);
                },
                child: Icon(
                  Icons.add,
                ),
              ),
        body: DoubleBackToCloseApp(
          child: RefreshIndicator(
            onRefresh: () async {
              reload();
            },
            child: FutureBuilder<List<Document>>(
              future: futureDocumentList,
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data.length > 0) {
                    return Column(
                      children: <Widget>[
                        MyTextInput(
                          label: AppLocalizations.of(context).get('keyword'),
                          inputController: _filterController,
                        ),
                        MyDivider(EdgeInsets.symmetric(vertical: MySpacer.XL)),
                        Expanded(
                          child: ListView.builder(
                            scrollDirection: Axis.vertical,
                            shrinkWrap: true,
                            itemCount: documentsList.length,
                            itemBuilder: (context, i) =>
                                filter == null || filter == ""
                                    ? DocumentCard(
                                        document: documentsList[i],
                                        index: i,
                                        longPressEnabled: longPressFlag,
                                        filter: false,
                                        callback: () {
                                          if (indexList.contains(i)) {
                                            indexList.remove(i);
                                          } else {
                                            indexList.add(i);
                                          }

                                          longPress();
                                        },
                                        goTo: () {
                                          Navigator.push(
                                            context,
                                            MaterialPageRoute(
                                                builder: (context) =>
                                                    DocumentsDetail(
                                                        documentsList[i])),
                                          ).whenComplete(reload);
                                        },
                                      )
                                    : documentsList[i].contains(filter)
                                        ? new DocumentCard(
                                            document: documentsList[i],
                                            longPressEnabled: longPressFlag,
                                            filter: true,
                                            goTo: () {
                                              Navigator.push(
                                                context,
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        DocumentsDetail(
                                                            documentsList[i])),
                                              ).whenComplete(reload);
                                            },
                                          )
                                        : new Container(),
                          ),
                        ),
                      ],
                    );
                  } else {
                    return EmptyList(
                        AppLocalizations.of(context).get('documents'));
                  }
                } else if (snapshot.hasError) {
                  return EmptyList(
                      AppLocalizations.of(context).get('documents'));
                }
                return Center(child: CircularProgressIndicator());
              },
            ),
          ),
          snackBar: SnackBar(
            content: Text(AppLocalizations.of(context).get('closeApp')),
          ),
        ),
      ),
    );
  }
}
