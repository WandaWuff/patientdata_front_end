import 'dart:io';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/widgets/cards/imageCard.dart';
import 'package:patientdata/widgets/header.dart';
import 'package:patientdata/widgets/mainText.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:provider/provider.dart';
import 'documentsEdit.dart';

// ignore: must_be_immutable
class DocumentsDetail extends StatefulWidget {
  Document _document;

  DocumentsDetail(this._document);

  @override
  _DocumentsDetailState createState() => _DocumentsDetailState();
}

class _DocumentsDetailState extends State<DocumentsDetail> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  Future<File> file;

  @override
  void initState() {
    file = Provider.of<ApiProvider>(context, listen: false)
        .getFile(widget._document.id);

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    String formattedDate;
    if (widget._document.date != null) {
      formattedDate = DateFormat('dd.MM.yy').format(widget._document.date);
    }

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('detail')),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.edit),
            onPressed: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) => DocumentsEdit(widget._document)),
              ).then((value) {
                setState(() {
                  widget._document = value;
                });
              });
            },
          ),
          IconButton(
            icon: Icon(Icons.delete),
            onPressed:
                null /*() {
              MyDialog.showDialogWithDeleteFunction(
                context,
                AppLocalizations.of(context).get('deleteDocument'),
                    () {
                  _delete();
                  Navigator.of(context).pop();
                },
              );
            }*/
            ,
          ),
        ],
      ),
      body: Container(
        child: FutureBuilder<File>(
          future: file,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              if (snapshot.data == null) {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(MySpacer.L),
                      child: Header('${widget._document.title} ' +
                          AppLocalizations.of(context).get('from') +
                          ' $formattedDate'),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.symmetric(horizontal: MySpacer.L),
                      child: MainText(widget._document.description),
                    ),
                    MyDivider(EdgeInsets.symmetric(vertical: MySpacer.L)),
                    Icon(Icons.broken_image),
                    MyDivider(EdgeInsets.only(top: MySpacer.L)),
                  ],
                );
              } else {
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(MySpacer.L),
                      child: Header('${widget._document.title} ' +
                          AppLocalizations.of(context).get('from') +
                          ' $formattedDate'),
                    ),
                    Padding(
                      padding:
                          const EdgeInsets.symmetric(horizontal: MySpacer.L),
                      child: MainText(widget._document.description),
                    ),
                    MyDivider(EdgeInsets.symmetric(vertical: MySpacer.L)),
                    ImageCard(snapshot.data),
                    MyDivider(EdgeInsets.only(top: MySpacer.L)),
                  ],
                );
              }
            }

            return Center(child: CircularProgressIndicator());
          },
        ),
      ),
    );
  }
}
