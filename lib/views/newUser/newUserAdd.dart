import 'package:flutter/material.dart';
import 'package:patientdata/models/user.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';
import 'package:provider/provider.dart';

class NewUserAdd extends StatefulWidget {
  @override
  _NewUserAddState createState() => _NewUserAddState();
}

class _NewUserAddState extends State<NewUserAdd> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _passwordController = TextEditingController();
  final _repeatPasswordController = TextEditingController();

  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _emailFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();

  void _addNewUser() {
    if (_passwordController.text != _repeatPasswordController.text) {
      MyDialog.showDialogWithParam(
          context, AppLocalizations.of(context).get('passwordNotSame'));
      return;
    }

    if (_firstNameController.text.isEmpty ||
        _lastNameController.text.isEmpty ||
        _emailController.text.isEmpty ||
        _passwordController.text.isEmpty |
            _repeatPasswordController.text.isEmpty) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedFirstName =
        Validation.trimLastSpace(_firstNameController.text);
    String validatedLastName =
        Validation.trimLastSpace(_lastNameController.text);
    String validatedEmail = Validation.trimLastSpace(_emailController.text);
    String validatedPassword =
        Validation.trimLastSpace(_passwordController.text);

    User user = new User(
      email: validatedEmail,
      firstName: validatedFirstName,
      lastName: validatedLastName,
      password: validatedPassword,
      roles: ["USER"],
    );

    Provider.of<ApiProvider>(context, listen: false)
        .callAddNewUser(user)
        .then((value) {
      if (value == 201) {
        Navigator.pop(context, true);
      } else {
        _scaffoldKey.currentState.showSnackBar(SnackBar(
          content: new Text(AppLocalizations.of(context).get('failed')),
          duration: Duration(seconds: 3),
        ));
        return;
      }
    });
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: Text(AppLocalizations.of(context).get('addUser')),
      ),
      body: Container(
        child: ListView(
          children: <Widget>[
            MyTextInput(
              label: AppLocalizations.of(context).get('firstName'),
              inputController: _firstNameController,
              inputAction: TextInputAction.next,
              focusNode: _firstNameFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _firstNameFocus, _lastNameFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('lastName'),
              inputController: _lastNameController,
              inputAction: TextInputAction.next,
              focusNode: _lastNameFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _lastNameFocus, _emailFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('email'),
              inputController: _emailController,
              inputAction: TextInputAction.next,
              focusNode: _emailFocus,
              onFieldSubmitted: (term) {
                _fieldFocusChange(context, _emailFocus, _passwordFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('password'),
              obscureText: true,
              inputController: _passwordController,
              inputAction: TextInputAction.done,
              focusNode: _passwordFocus,
              maxLines: 1,
              onFieldSubmitted: (term) {
                _fieldFocusChange(
                    context, _passwordFocus, _repeatPasswordFocus);
              },
            ),
            MyTextInput(
              label: AppLocalizations.of(context).get('againPassword'),
              obscureText: true,
              inputController: _repeatPasswordController,
              inputAction: TextInputAction.done,
              focusNode: _repeatPasswordFocus,
              maxLines: 1,
            ),
            MyDivider(EdgeInsets.only(top: MySpacer.XL)),
            MyRaisedButton(
              text: AppLocalizations.of(context).get('saveButton'),
              icon: Icons.save,
              function: _addNewUser,
            ),
          ],
        ),
      ),
    );
  }
}
