import 'package:flutter/material.dart';
import 'package:patientdata/models/user.dart';
import 'package:patientdata/validation/validation.dart';
import 'package:patientdata/values/appLocalizations.dart';
import 'package:patientdata/values/colors.dart';
import 'package:patientdata/values/spacer.dart';
import 'package:patientdata/views/logInView.dart';
import 'package:patientdata/views/registerView.dart';
import 'package:patientdata/widgets/dialog.dart';
import 'package:patientdata/widgets/divider.dart';
import 'package:patientdata/widgets/header.dart';
import 'package:patientdata/widgets/hyperlink.dart';
import 'package:patientdata/widgets/raisedButton.dart';
import 'package:patientdata/widgets/textInput.dart';

class CreateUserView extends StatefulWidget {
  static const String routeName = '/createUserView';

  @override
  _CreateUserViewState createState() => _CreateUserViewState();
}

class _CreateUserViewState extends State<CreateUserView> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  final _emailController = TextEditingController();
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _passwordController = TextEditingController();
  final _repeatPasswordController = TextEditingController();

  final FocusNode _emailFocus = FocusNode();
  final FocusNode _firstNameFocus = FocusNode();
  final FocusNode _lastNameFocus = FocusNode();
  final FocusNode _passwordFocus = FocusNode();
  final FocusNode _repeatPasswordFocus = FocusNode();

  User user;

  void _register() {
    //TODO: better password validation like minimum count of char or something
    if (_passwordController.text != _repeatPasswordController.text) {
      MyDialog.showDialogWithParam(
          context, AppLocalizations.of(context).get('passwordNotSame'));
      return;
    }


    if (_passwordController.text.length < 6) {
      MyDialog.showDialogWithParam(
          context, AppLocalizations.of(context).get('minChar'));
      return;
    }

    if (_firstNameController.text.isEmpty ||
        _lastNameController.text.isEmpty ||
        _emailController.text.isEmpty ||
        _passwordController.text.isEmpty ||
        _repeatPasswordController.text.isEmpty) {
      MyDialog.showDialogWithNoEmptyFields(context);
      return;
    }

    String validatedFirstName =
        Validation.trimLastSpace(_firstNameController.text);
    String validatedLastName =
        Validation.trimLastSpace(_lastNameController.text);
    String validatedEmail = Validation.trimLastSpace(_emailController.text.replaceAll("\@", "AT"));

    User user = new User(
      email: validatedEmail,
      firstName: validatedFirstName,
      lastName: validatedLastName,
      password: _passwordController.text,
      roles: ["USER", "ADMIN"],
    );

    Navigator.push(context,
        MaterialPageRoute(builder: (context) => RegisterView(user: user)));
  }

  _fieldFocusChange(
      BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
    currentFocus.unfocus();
    FocusScope.of(context).requestFocus(nextFocus);
  }

  // ignore: missing_return
  Future<bool> _backPressed() {
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => LogInView()));
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _backPressed,
      child: Scaffold(
        key: _scaffoldKey,
        body: Container(
          color: MyColors.PrimaryColor,
          child: Center(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.only(
                        top: MySpacer.XXXL,
                        left: MySpacer.XXXL,
                        right: MySpacer.XXXL),
                    child: Card(
                      elevation: 20,
                      color: MyColors.White,
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(5.0),
                          side: BorderSide(color: MyColors.PrimaryDarkColor)),
                      child: Padding(
                        padding: const EdgeInsets.all(MySpacer.XXL),
                        child: Column(
                          children: <Widget>[
                            Header(
                              AppLocalizations.of(context).get('createAccount'),
                              color: MyColors.PrimaryDarkColor,
                            ),
                            MyDivider(
                              EdgeInsets.only(
                                  top: MySpacer.XXL, bottom: MySpacer.S),
                            ),
                            MyTextInput(
                              label:
                                  AppLocalizations.of(context).get('firstName'),
                              inputController: _firstNameController,
                              inputAction: TextInputAction.next,
                              focusNode: _firstNameFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _firstNameFocus, _lastNameFocus);
                              },
                            ),
                            MyTextInput(
                              label:
                                  AppLocalizations.of(context).get('lastName'),
                              inputController: _lastNameController,
                              inputAction: TextInputAction.next,
                              focusNode: _lastNameFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _lastNameFocus, _emailFocus);
                              },
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context).get('email'),
                              inputController: _emailController,
                              inputAction: TextInputAction.next,
                              focusNode: _emailFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(
                                    context, _emailFocus, _passwordFocus);
                              },
                            ),
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                  vertical: MySpacer.S),
                            ),
                            MyTextInput(
                              label:
                                  AppLocalizations.of(context).get('password'),
                              obscureText: true,
                              inputController: _passwordController,
                              inputAction: TextInputAction.next,
                              focusNode: _passwordFocus,
                              onFieldSubmitted: (term) {
                                _fieldFocusChange(context, _passwordFocus,
                                    _repeatPasswordFocus);
                              },
                              maxLines: 1,
                            ),
                            MyTextInput(
                              label: AppLocalizations.of(context)
                                  .get('againPassword'),
                              obscureText: true,
                              inputController: _repeatPasswordController,
                              inputAction: TextInputAction.done,
                              focusNode: _repeatPasswordFocus,
                              maxLines: 1,
                            ),
                            MyDivider(EdgeInsets.only(top: MySpacer.L)),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: MyRaisedButton(
                                    text: AppLocalizations.of(context)
                                        .get('registerButton'),
                                    function: _register,
                                  ),
                                ),
                              ],
                            ),
                            MyDivider(
                              EdgeInsets.only(
                                bottom: MySpacer.XL,
                              ),
                            ),
                            Hyperlink(
                              text: AppLocalizations.of(context)
                                  .get('accountExists'),
                              function: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => LogInView()));
                              },
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
