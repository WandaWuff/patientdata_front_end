class ICD {
  String id;
  String text;

  ICD({
    this.id,
    this.text,
  });

  factory ICD.fromJson(Map<String, dynamic> json) {
    return ICD(
      id: json['Name'],
      text: json['Description'],
    );
  }
}
