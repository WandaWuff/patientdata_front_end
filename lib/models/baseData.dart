class BaseData {
  final String id;
  final String firstName;
  final String lastName;
  final String gender;
  final String bloodFormula;
  final DateTime birthDay;
  final String birthCity;
  final String street;
  final String plz;
  final String city;
  final String insurance;
  final String insuranceNumber;
  final String email;

  BaseData({
    this.id,
    this.firstName,
    this.lastName,
    this.gender,
    this.bloodFormula,
    this.birthDay,
    this.birthCity,
    this.street,
    this.plz,
    this.city,
    this.insurance,
    this.insuranceNumber,
    this.email,
  });

  getFullName() {
    return '$firstName $lastName';
  }

  getIdentifier() {
    return '${firstName.substring(0, 2)}${lastName.substring(0, 2)}${birthCity.substring(0, 2)}${birthDay.year}';
  }
}
