import 'dart:io';

class Image {
  final String id;
  final String documentsId;
  final File image;

  Image({
    this.id,
    this.documentsId,
    this.image,
  });
}
