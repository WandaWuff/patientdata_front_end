class Tenant {
  final String domain;
  final String schemaName;

  Tenant({
    this.domain,
    this.schemaName,
  });

  Map<String, dynamic> toJson() => {
    "domain": domain};
}
