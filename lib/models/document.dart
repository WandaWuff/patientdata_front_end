class Document {
  final String id;
   String title;
   String author;
   String description;
   DateTime date;

  Document({
    this.id,
    this.title,
    this.author,
    this.description,
    this.date,
  });

  setDocument(Document document) {
    this.title = document.title;
    this.author = document.author;
    this.description = document.description;
    this.date = document.date;
  }

  contains(String filter) {
    if (title.toLowerCase().contains(filter.toLowerCase()) ||
        author.toLowerCase().contains(filter.toLowerCase()) ||
        description.toLowerCase().contains(filter.toLowerCase())) {
      return true;
    } else {
      return false;
    }
  }

  factory Document.fromJson(Map<String, dynamic> json) {
    return Document(
      id: json['id'].toString(),
      title: json['title'],
      author: json['author'],
      description: json['description'],
      date: DateTime.fromMillisecondsSinceEpoch(json['date'].toInt() * 1000),
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "title": title,
    "author": author,
    "description": description,
    "date": date.toIso8601String() + 'Z',
  };
}
