class Diagnosis {
  String id;
  String icd;
  DateTime date;
  String place;
  String doctor;
  String text;

  Diagnosis({
    this.id,
    this.icd,
    this.date,
    this.place,
    this.doctor,
    this.text,
  });

  setText(String text) {
    this.text = text;
  }

  setDiagnosis(Diagnosis diagnosis) {
    this.icd = diagnosis.icd;
    this.date = diagnosis.date;
    this.place = diagnosis.place;
    this.doctor = diagnosis.doctor;
    this.text = diagnosis.text;
  }

  contains(String filter) {
    if (text == null) {
      if (icd.toLowerCase().contains(filter.toLowerCase()) ||
          place.toLowerCase().contains(filter.toLowerCase()) ||
          doctor.toLowerCase().contains(filter.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    } else {
      if (icd.toLowerCase().contains(filter.toLowerCase()) ||
          place.toLowerCase().contains(filter.toLowerCase()) ||
          text.toLowerCase().contains(filter.toLowerCase()) ||
          doctor.toLowerCase().contains(filter.toLowerCase())) {
        return true;
      } else {
        return false;
      }
    }

  }

  factory Diagnosis.fromJson(Map<String, dynamic> json) {
    return Diagnosis(
      id: json['id'].toString(),
      icd: json['icd'],
      date: DateTime.fromMillisecondsSinceEpoch(json['date'].toInt() * 1000),
      place: json['place'],
      doctor: json['doctor'],
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "icd": icd,
    "date": date.toIso8601String() + 'Z',
    "place": place,
    "doctor": doctor,
  };

}
