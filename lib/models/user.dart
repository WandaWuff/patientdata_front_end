class User {
  final String email;
  final String firstName;
  final String lastName;
  final String password;
  final List<String> roles;

  User({
    this.email,
    this.firstName,
    this.lastName,
    this.password,
    this.roles,
  });

  getFullName() {
    return '$firstName $lastName';
  }

  Map<String, dynamic> toJson() => {
        "firstName": firstName,
        "lastName": lastName,
        "email": email,
        "password": password,
        "roles": roles,
      };
}
