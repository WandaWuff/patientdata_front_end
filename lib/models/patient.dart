class Patient {
  final String id;
  String prename;
  String lastname;
  String email;
  String sex;
  DateTime dateOfBirth;
  String placeOfBirth;
  String healthInsurance;
  String bloodFormula;


  Patient({
    this.id,
    this.prename,
    this.lastname,
    this.email,
    this.sex,
    this.dateOfBirth,
    this.placeOfBirth,
    this.healthInsurance,
    this.bloodFormula,
  });

  getFullName() {
    return '$prename $lastname';
  }

  setPatient(Patient patient) {
    this.prename = patient.prename;
    this.lastname = patient.lastname;
    this.email = patient.email;
    this.sex = patient.sex;
    this.dateOfBirth = patient.dateOfBirth;
    this.placeOfBirth = patient.placeOfBirth;
    this.healthInsurance = patient.healthInsurance;
    this.bloodFormula = patient.bloodFormula;
  }

  factory Patient.fromJson(Map<String, dynamic> json) {
    return Patient(
      id: json['id'].toString(),
      prename: json['prename'],
      lastname: json['lastname'],
      email: json['email'],
      sex: json['sex'],
      dateOfBirth: DateTime.fromMillisecondsSinceEpoch(json['dateOfBirth'].toInt() * 1000),
      placeOfBirth: json['placeOfBirth'],
      healthInsurance: json['healthInsurance'],
      bloodFormula: json['bloodFormula'],
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "prename": prename,
    "lastname": lastname,
    "email": email,
    "sex": sex,
    "dateOfBirth": dateOfBirth.toIso8601String() + 'Z',
    "placeOfBirth": placeOfBirth,
    "healthInsurance": healthInsurance,
    "bloodFormula": bloodFormula,
  };
}
