class EmergencyContact {
  final String id;
  String preName;
  String lastName;
  String email;

  EmergencyContact({
    this.id,
    this.preName,
    this.lastName,
    this.email,
  });

  setEmergencyContact(EmergencyContact emergencyContact) {
    this.preName = emergencyContact.preName;
    this.lastName = emergencyContact.lastName;
    this.email = emergencyContact.email;
  }

  getFullName() {
    return '$preName $lastName';
  }

  contains(String filter) {
    if (lastName.toLowerCase().contains(filter.toLowerCase()) ||
        email.toLowerCase().contains(filter.toLowerCase()) ||
        preName.toLowerCase().contains(filter.toLowerCase())) {
      return true;
    } else {
      return false;
    }
  }

  factory EmergencyContact.fromJson(Map<String, dynamic> json) {
    return EmergencyContact(
      id: json['id'].toString(),
      preName: json['prename'],
      lastName: json['lastname'],
      email: json['email'],
    );
  }

  Map<String, dynamic> toJson() => {
    "id": id,
    "prename": preName,
    "lastname": lastName,
    "email": email,
  };
}
