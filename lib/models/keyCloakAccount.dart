class KeyCloakAccount {
  final String clientId;
  final String username;
  final String password;
  final String grantType;

  KeyCloakAccount({
    this.clientId,
    this.username,
    this.password,
    this.grantType,
  });

  Map<String, dynamic> toJson() => {
    "client_id": clientId,
    "username": username,
    "password": password,
    "grant_type": grantType,
  };

  String toPostString() {
    return "client_id=patientdata-frontend&username=$username&password=$password&grant_type=password";
  }
}
