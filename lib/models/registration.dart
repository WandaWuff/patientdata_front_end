import 'package:patientdata/models/patient.dart';
import 'package:patientdata/models/tenant.dart';
import 'package:patientdata/models/user.dart';

class Registration {
  final Tenant tenant;
  final User user;
  final Patient patient;

  Registration({
    this.tenant,
    this.user,
    this.patient,
  });

  Map<String, dynamic> toJson() => {
        "tenant": tenant,
        "user": user,
        "patient": patient,
      };
}
