import 'dart:convert';
import 'dart:io';
import 'package:flutter/foundation.dart';
import 'package:path_provider/path_provider.dart';
import 'package:patientdata/models/diagnosis.dart';
import 'package:patientdata/models/document.dart';
import 'package:patientdata/models/emergencyContact.dart';
import 'package:patientdata/models/icd.dart';
import 'package:patientdata/models/keyCloakAccount.dart';
import 'package:patientdata/models/patient.dart';
import 'package:patientdata/models/registration.dart';
import 'package:patientdata/models/user.dart';
import 'package:patientdata/values/requestAddress.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

class ApiProvider with ChangeNotifier {
  SharedPreferences pf;
  String name;

  //TODO: tidy up..
  Future<int> callKeyCloak(KeyCloakAccount kca, String domain) async {
    var url;
    String tenant;

    if (domain.isEmpty) {
      url =
          '${RequestAddress.keyCloak}/${kca.username}/protocol/openid-connect/token';
      tenant = kca.username;
    } else {
      url = '${RequestAddress.keyCloak}/$domain/protocol/openid-connect/token';
      tenant = domain;
    }

    final headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers':
          'Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers',
      'Content-Type': 'application/x-www-form-urlencoded',
    };
    String jsonBody = kca.toPostString();
    final encoding = Encoding.getByName('utf-8');

    http.Response response = await http.post(
      url,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    );

    if (response.statusCode == 200) {
      SharedPreferences prefs = await SharedPreferences.getInstance();

      prefs.setString('status', 'loggedIn');
      prefs.setString(
          'accessToken', parseResponse(response.body)["access_token"]);
      prefs.setString('username', kca.username);
      prefs.setString('password', kca.password);
      prefs.setString('tenant', tenant);
    }

    return response.statusCode;
  }

  Future<String> callKeyCloakAndReturnToken(KeyCloakAccount kca) async {
    var url =
        '${RequestAddress.keyCloak}/${kca.username}/protocol/openid-connect/token';

    final headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Content-Type': 'application/x-www-form-urlencoded',
    };

    String jsonBody = kca.toPostString();
    final encoding = Encoding.getByName('utf-8');

    http.Response response = await http.post(
      url,
      headers: headers,
      body: jsonBody,
      encoding: encoding,
    );

    String responseBody = response.body;

    return parseResponse(responseBody)["access_token"];
  }

  Map<String, dynamic> parseResponse(String response) {
    return jsonDecode(response);
  }

  Future<int> callRegister(Registration registration) async {
    var url = RequestAddress.register;

    var response = await http.post(url,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
          'Access-Control-Allow-Headers': 'X-Requested-With',
          'Content-Type': 'application/json',
        },
        body: json.encode(registration));

    return response.statusCode;
  }

  Future<String> fetchICD(String icd) async {
    var uri = 'http://icd10api.com/?desc=short&r=json&code=$icd';
    var response;
    response = await http.get(uri);

    if (response.statusCode == 200) {
      return ICD.fromJson(json.decode(response.body)).text;
    } else {
      return null;
    }
  }

  // ignore: missing_return
  Future<Patient> getPatientAndSetFullName() async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.patient;

    try {
      final response = await http.get(url, headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      });

      Patient pat = Patient.fromJson(jsonDecode(response.body));

      if (response.statusCode == 200) {
        pf.setString('name', '${pat.getFullName()}');
      }

      return pat;
    } catch (error) {
      print('token invalid'); //
      return error; // executed for errors of all types other than Exception
    }
  }

  Future<int> callEditPatient(Patient patient) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.patient;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(patient),
    );

    return response.statusCode;
  }

  Future<int> callAddNewUser(User user) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String username = pf.getString('username');

    String url = RequestAddress.users;

    final response = await http.post(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': username,
      },
      body: json.encode(user),
    );

    return response.statusCode;
  }

  Future<List<Diagnosis>> getDiagnosisList() async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    List<Diagnosis> diagnosisList = new List();

    String url = RequestAddress.diagnoses;

    final response = await http.get(url, headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    });

    if (response.statusCode == 200) {
      diagnosisList = (json.decode(response.body) as List)
          .map((i) => Diagnosis.fromJson(i))
          .toList();
      return diagnosisList;
    } else {
      return null;
    }
  }

  Future<int> callAddDiagnosis(Diagnosis diagnosis) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.diagnoses;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(diagnosis),
    );

    return response.statusCode;
  }

  Future<int> callEditDiagnosis(Diagnosis diagnosis) async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.diagnoses;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(diagnosis),
    );

    return response.statusCode;
  }

  Future<List<EmergencyContact>> getEmergencyContactList() async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    List<EmergencyContact> emergencyContactList = new List();

    String url = RequestAddress.emergencyContacts;

    final response = await http.get(url, headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    });

    if (response.statusCode == 200) {
      emergencyContactList = (json.decode(response.body) as List)
          .map((i) => EmergencyContact.fromJson(i))
          .toList();
      return emergencyContactList;
    } else {
      return null;
    }
  }

  Future<int> callAddEmergencyContact(EmergencyContact emergencyContact) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.emergencyContacts;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(emergencyContact),
    );

    return response.statusCode;
  }

  Future<int> callEditEmergencyContact(
      EmergencyContact emergencyContact) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.emergencyContacts;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(emergencyContact),
    );

    return response.statusCode;
  }

  Future<List<Document>> getDocumentList() async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    List<Document> documentList = new List();

    String url = RequestAddress.documents;

    final response = await http.get(url, headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    });

    if (response.statusCode == 200) {
      documentList = (json.decode(response.body) as List)
          .map((i) => Document.fromJson(i))
          .toList();
      return documentList;
    } else {
      return null;
    }
  }

  Future<int> callAddDocument(Document document) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.documents;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(document),
    );

    return response.statusCode;
  }

  Future<int> callPostFile(String uuid, File file) async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    var url = Uri.parse('${RequestAddress.documents}/$uuid/file');
    var request = new http.MultipartRequest("PUT", url);

    Map<String, String> headers = {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Accept': '*/*',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    };

    request.files.add(await http.MultipartFile.fromPath(
      'file',
      file.path,
    ));

    request.headers.addAll(headers);

    final response = await request.send();

    return response.statusCode;
  }

  Future<int> callPostFileToDiagnosis(String uuid, Document document) async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = '${RequestAddress.diagnoses}/$uuid/documents';

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(document),
    );

    return response.statusCode;
  }

  Future<int> callEditDocument(Document document) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = RequestAddress.documents;

    final response = await http.put(
      url,
      headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      },
      body: json.encode(document),
    );

    return response.statusCode;
  }

  Future<File> getFile(String uuid) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    String url = '${RequestAddress.documents}/$uuid/file';

    final response = await http.get(url, headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    });

    if (response.statusCode == 200) {
      String dir = (await getApplicationDocumentsDirectory()).path;
      File file = new File('$dir/$uuid');
      await file.writeAsBytes(response.bodyBytes);
      return file;
    } else {
      return null;
    }
  }

  Future<List<File>> getFiles(List<Document> idList) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    List<File> listFile = new List();

    for (int i = 0; i < idList.length; i++) {
      String url = '${RequestAddress.documents}/${idList[i].id}/file';

      final response = await http.get(url, headers: {
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
        'Access-Control-Allow-Headers': 'X-Requested-With',
        'Authorization': 'Bearer $accessToken',
        'X-Tenant': tenant,
      });

      if (response.statusCode == 200) {
        String dir = (await getApplicationDocumentsDirectory()).path;
        File file = new File('$dir/${idList[i].id}');
        await file.writeAsBytes(response.bodyBytes).then((value) {
          listFile.add(file);
        });
      }
    }

    return listFile;
  }

  Future<List<Document>> getDocumentListFromDiagnosis(String uuid) async {
    pf = await SharedPreferences.getInstance();
    String accessToken = pf.getString('accessToken');
    String tenant = pf.getString('tenant');

    List<Document> documentList = new List();

    String url = '${RequestAddress.diagnoses}/$uuid/documents';

    final response = await http.get(url, headers: {
      'Access-Control-Allow-Origin': '*',
      'Access-Control-Allow-Methods': 'POST,GET,DELETE,PUT,OPTIONS',
      'Access-Control-Allow-Headers': 'X-Requested-With',
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Bearer $accessToken',
      'X-Tenant': tenant,
    });

    if (response.statusCode == 200) {
      documentList = (json.decode(response.body) as List)
          .map((i) => Document.fromJson(i))
          .toList();
      return documentList;
    } else {
      return null;
    }
  }
}
