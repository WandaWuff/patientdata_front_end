import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:patientdata/providers/apiProvider.dart';
import 'package:patientdata/views/createUserView.dart';
import 'package:patientdata/views/emergencyContacts/emergencyContactList.dart';
import 'package:patientdata/routes/routes.dart';
import 'package:patientdata/views/logInView.dart';
import 'package:patientdata/views/settingView.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import './values/colors.dart';
import 'values/appLocalizations.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:flutter/cupertino.dart';
import 'views/baseData/baseDataOverview.dart';
import 'cupertinoLocalizations.dart';
import 'views/diagnosis/diagnosisList.dart';
import 'views/documents/documentsList.dart';

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  // Icons erstellt von <a href="https://www.flaticon.com/de/autoren/flat-icons" title="Flat Icons">Flat Icons</a> from <a href="https://www.flaticon.com/de/" title="Flaticon"> www.flaticon.com</a>
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String status;

  Future<void> getLoginState() async {
    SharedPreferences pf = await SharedPreferences.getInstance();
    String loginState = pf.getString('status');

    setState(() {
      status = loginState;
    });
  }

  @override
  void initState() {
    getLoginState();

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider<ApiProvider>(create: (_) => ApiProvider()),
      ],
      child: MaterialApp(
        supportedLocales: [
          const Locale('en', 'US'),
          const Locale('de', 'DE'), // English
        ],
        localizationsDelegates: [
          const AppLocalizationDelegate(),
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
          DefaultCupertinoLocalizations.delegate,
          GermanCupertinoLocalizations.delegate,
        ],
        debugShowCheckedModeBanner: false,
        title: 'Precious health data',
        theme: ThemeData(
          primarySwatch: MyColors.PrimaryColor,
          accentColor: MyColors.PrimaryAccentColor,
          cardColor: MyColors.PrimaryColorExtraLight,
        ),
        home: status == 'loggedIn' ? BaseDataOverview() : LogInView(),
        routes: {
          Routes.baseData: (context) => BaseDataOverview(),
          Routes.diagnosis: (context) => DiagnosisList(),
          Routes.documents: (context) => DocumentsList(),
          Routes.settingView: (context) => SettingView(),
          Routes.emergencyView: (context) => EmergencyContactList(),
          Routes.loginView: (context) => LogInView(),
          Routes.createUserView: (context) => CreateUserView(),
        },
      ),
    );
  }
}
